//app.js
// var hotapp = require('utils/hotapp.js');
var uTil = require('/utils/util.js');
var apiM = require('/utils/APIManager.js');
App({
  onLaunch: function () {
    //调用API从本地缓存中获取数据
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())

      apiM.getCityInfo(function (res) {
        wx.setStorageSync("cityinfo", res)
      });
  
    wx.getUserInfo({
      success: function (res) {
        wx.setStorageSync("wxuserinfo", res.userInfo);
      }
    })
   

  },

  onShow: function () {    
  },
  updateBizinfo: function (callback) {
      apiM.getSessionId(function () {
        apiM.getBizInfo(function (bizinfo) {
          wx.setStorageSync("bizinfo", bizinfo);
          callback();
        })
      });
    
  },
  onHide: function () {
    console.log('App onHide');
  },
  onError: function () {
    console.log('App onError');
  },

  getPics: function () {
    return this.globalData.picList;
  },


  globalData: {
    userInfo: null,
    openid: 'openid',
    apikey: '8a53b78c56541fb00156541fb0760000',
    ajaxUrl: ' http://download.dingdatech.com/router.html',
    locationInfo: null
  },
  data: {
    fromid: 0
  }
})