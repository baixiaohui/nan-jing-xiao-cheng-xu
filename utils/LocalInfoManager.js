var stakeTyp = function (){
    console.log("开始判断本地是否存在有无桩信息");
    var tmp = wx.getStorageSync("isHaveStake");
    if (!tmp) {
      console.log("本地没有 有无桩信息 默认设置有桩");
      wx.setStorageSync("isHaveStake", 1);
    }else {
      console.log("本地有 有无桩信息 ");
    }
  
}
var searchHistorysave = function(stake,stationinf){
  console.log("添加历史记录");
  var histtoryData;
  if (stake==1){
     histtoryData = wx.getStorageSync("history");
  }else{
    histtoryData = wx.getStorageSync("withoutstakehistory");
  }
    if (histtoryData) 
    {
      console.log("有历史记录、直接添加");
      for(var i =0;i<histtoryData.length;i++){
        if (stake == 1) {
          if (histtoryData[i].number == stationinf.number) {
            return;
          }
        }else{
          if (histtoryData[i].deviceId == stationinf.deviceId) {
            return;
          }
        }
      }
      console.log("存入本地");
      histtoryData.push(stationinf);
      if (stake == 1){
        wx.setStorageSync("history", histtoryData); 
      }else{
        wx.setStorageSync("withoutstakehistory", histtoryData); 
      }
    }else{
      var history = [];
      console.log("无历史记录、创建存储");
      history.push(stationinf);
      if (stake == 1) {
        wx.setStorageSync("history", history);
      } else {
        wx.setStorageSync("withoutstakehistory", history);
      }
    }

}

var readHistory = function(stake,callback){
  console.log("获取历史记录");
  var histtoryData;
  if (stake==1){
   histtoryData = wx.getStorageSync("history");
  }else{
    histtoryData = wx.getStorageSync("withoutstakehistory");
  }
  if (histtoryData) {
  callback(histtoryData);
  }else{
    console.log("暂无历史记录");
  }
}
var cleanHistory = function(stake){
  console.log("清除历史记录");
  if (stake == 1) {
    wx.removeStorageSync("history");
  } else {
    wx.removeStorageSync("withoutstakehistory");
  }
}
module.exports = {
  stakeTyp: stakeTyp,
  searchHistorysave: searchHistorysave,
  readHistory: readHistory,
  cleanHistory: cleanHistory
}