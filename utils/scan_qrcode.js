var urlManager = require('/URLManager.js')
var urlObject = urlManager.getUrlObject();
var connectBleFun = require('../pages/conn/conn.js');
var apiM = require('/APIManager.js');
var isConnectedDeviced = false;
//扫码
function scan_code(qrscancallback){
  rentToCode(qrscancallback);
  
}
//扫码租车
function rentToCode(rentToCodeBack) {
  var status = statusJudge();
  console.log("status:" + status);
  if (status == false) {
    return;
  }


  wx.scanCode({
    success: function (res) {
      // console.log(res);
      var reg = /(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&:/~\+#]*[\w\-\@?^=%&/~\+#])?/;
      console.log(res.result);
      console.log("是否无桩:" + reg.test(res.result));
      clearTime('租车');
      wx.removeStorageSync('blueDevice_id');
      wx.removeStorageSync('blueDevice_name');
      if (res.result.length == 11) {
        //有桩
        scanCode(markerResult(res.result), rentToCodeBack);
      }
      else if (reg.test(res.result) && res.result.search('citycode') != -1 && res.result.search('sn') != -1 && res.result.search('biztype' != -1)) {
        //蓝牙
        var result = resultObject(res.result);
        if (result) {
          dianziLan(rentToCodeBack, 'rent', result);
          // blueToCar(result, 'rent', rentToCodeBack)//测试，不检测电子桩
        } else {
          showMsg('扫描的不是租车二维码');
        }

      } else {
        showMsg('扫描的不是租车二维码');
      }

    },
    fail: function (res) {
      console.log('wx.scan:');
      console.log(res);
    },
    complete: function () { }
  })
}
//关锁
function lockBike(lockCallbike) {
  clearTime('还车');
  apiM.getBizInfo(function (bizinfo) {
    if(bizinfo.bizStatus != '1004'){
      showMsg('车辆已还');
      wx.setStorageSync("bizinfo", bizinfo);
      lockCallbike({ 'rentType': '2', 'bizStatus': bizinfo.bizStatus });
    }else{
      
      if (wx.getStorageSync("isTemporaryBike") == "true") {
       wx.hideLoading()
       wx.showModal({
         title: '提示',
         content: '临时停车中不可还车！',
       })
     }else {
       dianziLan(lockCallbike, 'back', null)
      // backToCode(lockCallbike, 'back');//测试，不检测电子桩
     }
      
    }
  })
 
}
//临时停车
function temporaryParking(lockCallbike){
  
  wx.showModal({
    title: '提示',
    content: '临时停车仍在计费中，未避免产生超额费用，请及时还车!',
    success: function (res) {
      if (res.confirm) {
        clearTime('临时停车');
        apiM.getBizInfo(function (bizinfo) {
          if (bizinfo.bizStatus != '1004') {
            showMsg('车辆已还');
            wx.setStorageSync("bizinfo", bizinfo);
            lockCallbike({ 'rentType': '2', 'bizStatus': bizinfo.bizStatus });
          } else {

            // dianziLan(lockCallbike, 'linshi', null)//电子桩
            backToCode(lockCallbike, 'linshi');//测试，不检测电子桩
          }
        })
      }
    }
  })
  
}
//继续骑行
function continueBike(continueBack){
  clearTime('开锁');
  apiM.getBizInfo(function (bizinfo) {
    if (bizinfo.bizStatus != '1004') {
      showMsg('车辆已还');
      wx.setStorageSync("bizinfo", bizinfo);
      lockCallbike({ 'rentType': '2', 'bizStatus': bizinfo.bizStatus });
    } else {

      backToCode(continueBack, 'jixu');
    }
  })
 
}
//重置定时器
var timeOut;
function clearTime(title){
  clearTimeout(timeOut);
  // setTimeout(function(){
    wx.showLoading({
      title: title + '中...',
      mask: true
    })
    
 timeOut =  setTimeout(function () {
      
      if (isConnectedDeviced == false){
        showMsg('连接超时请重试');

      }else{
        wx.hideLoading();
      }
      
    }, 20000)
  // },1)
 

}
//电子桩
function dianziLan(dianziBack, caozuo, blueresult) {
  // blueToCar(blueresult, 'rent', dianziBack);

  // if (caozuo == 'rent') {
  //   blueToCar(blueresult, 'rent', dianziBack);
  // } else {
  //   backToCode(dianziBack);
  // }
//  return;

  // wx.getBluetoothAdapterState({
  //   success: function(res) {
  //     console.log('适配器状态',res);
  //     dianzizhuangScan(dianziBack, caozuo, blueresult);
  //   },
  //   fail:function(res){
  //     wx.openBluetoothAdapter({
  //       success: function (res) {
  //         dianzizhuangScan(dianziBack, caozuo, blueresult);
  //       },
  //       fail: function () {
  //         showMsg('请开启蓝牙后重试');

  //       }
  //     })
  //   },
  //   complete:function(res){
        

       
      
  //   }
  // })
  wx.closeBluetoothAdapter({
    success: function(res) {

    },
    complete: function(res){
      wx.openBluetoothAdapter({
        success: function (res) {
          dianzizhuangScan(dianziBack, caozuo, blueresult);
        },
        fail: function () {
          showMsg('请开启蓝牙后重试');

        }
      })
    }
  })
  
 
     
  
 
}
function IsNum(s) {
  if (s != null) {
    var r, re;
    re = /\d*/i; //\d表示数字,*表示匹配多个数字
    r = s.match(re);
    return (r == s) ? true : false;
  }
  return false;
}
//扫描电子桩
function dianzizhuangScan(dianziBack, caozuo, blueresult){
  var name;
  var errLog;
  var youzhuang = false;
  var timeCount = 10000;
  if (caozuo == 'rent') {
    // errLog = '不在租车范围内'
  } else if(caozuo == 'linshi'){
    timeCount = 5000;
    errLog = '不可在还车区域临时停车'
  }else if(caozuo == 'jixu'){
    
  }else if(caozuo == 'back'){
    errLog = '不在还车范围内'
  }
  var serviceID = '6e400001-b5a3-f393-e0a9-e50e24dcca9f';
  serviceID = serviceID.toUpperCase();
  wx.startBluetoothDevicesDiscovery({
    services: [serviceID],//

    success: function (res) {
      // success
      console.log("-----startBluetoothDevicesDiscovery--success----------", serviceID);
      wx.onBluetoothDeviceFound(function (res) {
        console.log('扫描到的电子桩', res);
        var resData = res.devices[0];
        // if (togetSysInfo() != 'ios') {
        //   resData = res.devices[0][0];
        // }
        youzhuang = true;
        name = resData.name

        var rssi = resData.RSSI
        if(IsNum(name)){
          wx.setStorageSync('deviceStakeId', name)
        }else{
          wx.setStorageSync('deviceStakeId', '')
        }
       
        wx.stopBluetoothDevicesDiscovery({
          success: function (res) {
            console.log(rssi);
            // if (rssi > -100) {//不检测信号强度（扫到电子桩即可）
              if (caozuo == 'rent') {
                blueToCar(blueresult, 'rent', dianziBack);
              } else if (caozuo == 'back') {
                backToCode(dianziBack, 'back');
              }
            // }
          },
          fail: function () {
            // showMsg(errLog);
            showMsg('扫描电子驿站失败');
          },
          complete:function(){
              console.log('电子桩扫描完成');
          }
        })
      })
      setTimeout(function () {
        if (youzhuang == false ) {
          if (caozuo  == 'linshi' || caozuo == 'rent'){
            backToCode(dianziBack, caozuo);
          }else{
            showMsg(errLog);
          }

        } else{
          if (caozuo == 'linshi') {
            showMsg(errLog);
          }
        }

      }, timeCount)
    },
    fail: function (res) {
      // fail
      console.log(res);
      // showMsg('扫描电子驿站失败');

    }
  })
  
}

//继续骑行，临时停车，还车
function backToCode(backToCodeback,status){
  if (wx.getStorageSync('blueDevice_id') && wx.getStorageSync('blueDevice_name') && togetSysInfo() != 'ios') {
    wx.closeBluetoothAdapter({
      success: function(res) {

      },
      complete: function(res){
        connecttobluforname(backToCodeback, status);
      }
    })
  } else {
    var bizInfo = wx.getStorageSync('bizinfo');
    console.log("bizInfo.bizExtra:::" + bizInfo.bizExtra);
    if (bizInfo.bizExtra.length > 5) {
      var bikeId = JSON.parse(bizInfo.bizExtra).bikeId;
      console.log("bikeId+++ppp::::::" + bikeId);
      // var bikeId = '10010010';
      if (bikeId) {
        var result = {
          sn: bikeId,
          citycode: '',
          biztype: '3'
        };
        if (status == 'linshi'){
          blueToCar(result, 'linshi', backToCodeback);
        } else if (status == 'back'){
          blueToCar(result, 'back', backToCodeback);
        } else if (status == 'jixu'){
          wx.closeBluetoothAdapter({
            success: function (res) { },
            complete: function (res) {
              blueToCar(result, 'jixu', backToCodeback);
            }
          })
          
        }
       

      } else {
        // wx.showToast({
        //   title: '还车失败请重试！！！',
        // })
        showMsg('操作失败请重试！！！');
      }
    }
    else {
      showMsg('操作失败请重试！！！');
     
    }
  }

}

//直连设备(扫描)
function connecttoblufornameToScan(connectback,operType) {
  // wx.showLoading({
  //   title: '还车中..',
  //   mask:true
  // })
  console.log('idANdeid::::', wx.getStorageSync('blueDevice_id')); 
  console.log('idANdeName::::', wx.getStorageSync('blueDevice_name'));
  wx.openBluetoothAdapter({
    success: function (res) {
      var serviceID = '6e400001-b5a3-f393-e0a9-e50e24dcca9f';
      serviceID = serviceID.toUpperCase();
      wx.startBluetoothDevicesDiscovery({
        services: [serviceID],
        success: function(res) {
          wx.onBluetoothDeviceFound(function(res){
            if (res.devices[0].name == wx.getStorageSync('blueDevice_name')){
              connectBleFun.scanPage(wx.getStorageSync('blueDevice_id'), wx.getStorageSync('blueDevice_name'), operType, '', connectback, function (res) {
                isConnectedDeviced = true;
                if (res.title == '请手动关锁') {
                  wx.playBackgroundAudio({
                    dataUrl: 'http://dingda-cdn01.oss-cn-hangzhou.aliyuncs.com/DingDaApk/ding.mp3',
                    success: function () {
                      console.log('ssssssss_________++=======');
                    },
                    fail: function (res) {
                      console.log('diididi_________++=======', res);
                    },
                    complete: function () {
                      console.log('-==========++=======');
                    }
                  })
                }
                wx.showToast({
                  title: res.title,
                  duration: 3000,
                  // mask: true,
                })
               

                if(res.success == 'false'){
                  wx.closeBluetoothAdapter({
                    success: function (res) {

                    },
                  })
                } else if (res.success == 'true'){
              wx.closeBLEConnection({
                deviceId: wx.getStorageSync('blueDevice_id'),
                success: function (res) { 
                  wx.closeBluetoothAdapter({
                    success: function (res) {

                    },
                  })
                },
              })
            }

              });
            }
            
          })
        },
      })
      
    },
    fail: function () {
      console.log('请开启蓝牙后重试');
      
      // wx.closeBluetoothAdapter({
      //   success: function(res) {},
      // })
    }
  })

}
//直连(不扫描)
function connecttobluforname(connectback, operType) {
  // wx.showLoading({
  //   title: '还车中..',
  //   mask:true
  // })
  isConnectedDeviced = false;
  wx.removeStorageSync('connectSu_jc');
  console.log('idANdeid::::', wx.getStorageSync('blueDevice_id'));
  console.log('idANdeName::::', wx.getStorageSync('blueDevice_name'));
  wx.openBluetoothAdapter({
    success: function (res) {
      connectBleFun.scanPage(wx.getStorageSync('blueDevice_id'), wx.getStorageSync('blueDevice_name'), operType, '', connectback, function (res) {
        isConnectedDeviced = true;
        if (res.title == '请手动关锁'){
          wx.playBackgroundAudio({
            dataUrl: 'http://dingda-cdn01.oss-cn-hangzhou.aliyuncs.com/DingDaApk/ding.mp3',
            success: function () {
              console.log('ssssssss_________++=======');
            },
            fail: function (res) {
              console.log('diididi_________++=======', res);
            },
            complete: function () {
              console.log('-==========++=======');
            }
          }) 
        }
        wx.showToast({
          title: res.title,
          duration: 3000,
          // mask: true,
        })

        if (res.success == 'false') {
          wx.closeBluetoothAdapter({
            success: function (res) {

            },
          })
        } else if (res.success == 'true') {
          wx.closeBLEConnection({
            deviceId: wx.getStorageSync('blueDevice_id'),
            success: function (res) {
              wx.closeBluetoothAdapter({
                success: function (res) {

                },
              })
            },
          })
        }

      });
      setTimeout(function () {
        
        console.log('isConnectedDeviced=====', wx.getStorageSync('connectSu_jc'));
        if (wx.getStorageSync('connectSu_jc') != 'connectSu_jc') {
          wx.closeBLEConnection({
            deviceId: wx.getStorageSync('blueDevice_id'),
            success: function(res) {
              connecttoblufornameToScan(connectback, operType);
            },
          })
         
        }
      }, 3500)

    },
    fail: function () {
      console.log('请开启蓝牙后重试');

      // wx.closeBluetoothAdapter({
      //   success: function(res) {},
      // })
    }
  })

}
//蓝牙锁扫码结果解析
function resultObject(result){
  var oneArr = result.split("?");
  var twoArr = oneArr[1].split("&");
  var object = { 'citycode': '', 'sn': '','biztype':''};
  object.citycode = twoArr[0].split('=')[1];
  object.sn = twoArr[1].split('=')[1];
  object.biztype = twoArr[2].split('=')[1];
  return object;
}
//蓝牙租车
function blueToCar(result, isClosed, bluerentcallback){
  

  // wx.closeBluetoothAdapter({
  //   success: function (res) {
  //     console.log('关闭资源',res);
  //   },
  // })
  //初始化蓝牙设备
  wx.openBluetoothAdapter({
    success: function (res) {
      // success
      console.log("openBluetoothAdapter-----success----------");
      // console.log(res);
      var sacnNum = 0;
      //开始搜索设备
      var serviceID = '6e400001-b5a3-f393-e0a9-e50e24dcca9e';
      serviceID = serviceID.toUpperCase();
      wx.startBluetoothDevicesDiscovery({
        services: [serviceID],
        success: function (res) {
          // success
          console.log("-----startBluetoothDevicesDiscovery--success----------");
        },
        fail: function (res) {
          // fail
          console.log(res);
       
        }
      })
      wx.onBluetoothDeviceFound(function (res) {
        console.log("onBluetoothDeviceFound");
        console.log(res);
        var resData = res.devices[0];
        // if (togetSysInfo() != 'ios'){
        //   resData = res.devices[0][0];
        // }
        console.log(resData.name);
        //蓝牙广播中二维码信息
        let buffer = resData.advertisData;
        let dataView = new DataView(buffer)
        let dataLength = dataView.byteLength
        console.log(dataView.byteLength);
        let dataResult = dataView.getUint8(dataLength - 4).toString(16);
        // console.log("拿到的锁状态" + dataView.getUint8(dataLength - 5).toString(16))
        
        for (var i = dataLength - 3; i < dataView.byteLength; i++) {
          var eachData = dataView.getUint8(i).toString(16);
          console.log('所有数据', dataView.getUint8(i).toString(16));
          if (eachData.length != 2) {
            eachData = '0' + eachData;
          }
          dataResult = dataResult + eachData;
        }
        console.log("拿到的所有数据" + dataResult)
        dataResult = parseInt(dataResult,16);
        console.log("拿到的数据" + dataResult)
        console.log("二维码的数据" + result.sn)
        sacnNum++;
        //二维码与蓝牙扫描结果进行匹配
        if( isClosed != 'rent'){
          dataResult = resData.name;
        }
        // wx.showModal({
        //   title: '车辆编号' + resData.name,
        //   content: '蓝牙广播的二维码' + dataResult,
        // })
        if (dataResult == result.sn) {
    
          var deviceId = resData.deviceId;
          console.log('匹配结果:'+deviceId);
          var name = resData.name;
          console.log(deviceId+'===='+name);
          
          connectBleFun.scanPage(deviceId, name, isClosed, result.citycode, bluerentcallback,function(res){
            isConnectedDeviced = true;
            if (res.title == '请手动关锁') {
              wx.playBackgroundAudio({
                dataUrl: 'http://dingda-cdn01.oss-cn-hangzhou.aliyuncs.com/DingDaApk/ding.mp3',
                success: function () {
                  console.log('ssssssss_________++=======');
                },
                fail: function (res) {
                  console.log('diididi_________++=======', res);
                },
                complete: function () {
                  console.log('-==========++=======');
                }
              })
            }
              wx.showToast({
                title: res.title,
                duration: 3000,
                // mask:true,
              })
            
         
              if (res.success == 'false') {
                wx.closeBluetoothAdapter({
                  success: function (res) {

                  },
                })
              } else if (res.success == 'true') {
                wx.closeBLEConnection({
                  deviceId: wx.getStorageSync('blueDevice_id'),
                  success: function (res) {
                    wx.closeBluetoothAdapter({
                      success: function (res) {

                      },
                    })
                  },
                })
              }
            
             
          }); 
        }      
      })

    },
    fail: function (res) {
      showMsg('请开启蓝牙后重试');
    }
  }) 
}
//有桩车扫码结果
function markerResult(result){
  var object = { 'cityId': '', 'stationId': '', 'parkingId': '' };
  object.cityId = parseInt(result.substring(0, 4), 36);
  object.stationId = parseInt(result.substring(4, 8), 36);
  object.parkingId = parseInt(result.substring(8, 10), 36);
  console.log(object);
  return object;
}
function clearDeviceId(){
  wx.setStorageSync("blue_deviceID", null);
}
//有桩租车请求
function scanCode(result,qrscan_back) {
  var sessionId = wx.getStorageSync('sessionId_');
  var url = urlObject.haveStakeRentUrl;
  var data = {
    sessionId_: sessionId,
    cityCode: result.cityId+'99',//线上要加99
    siteNum: result.stationId, //设备ID
    parkNum: result.parkingId //有桩租车墩
  };
  var successmethod = function (res) {
    
    console.log("scancode");
    console.log(res);
    switch (res.data.bizstatus){
      case "2002":
        showMsg("车位无车");
        break;
      case "2003":
        showMsg("开锁操作失败");
        break;
      case "2004":
        showMsg("网点不在线");
        break;
      case "2005":
        showMsg("城市代码不匹配");
        break;
      case "2006":
        showMsg("车位不存在");
        break;
      case "2007":
        showMsg("网点不存在");
        break;
      case "2008":
        showMsg("请尝试其他车位");
        break;
      case "2009":
        showMsg("非服务时间");
        break;
      case "0":
        showMsg('租车成功');
        
        var bizInfo = wx.getStorageSync("bizinfo");
        bizInfo.bizStatus = "1004";
        wx.setStorageSync("bizinfo", bizInfo);
        qrscan_back({'rentType':'1','bizStatus':'1004'});
        // callback("1004");
        break;
        default:
        console.log(res.data.bizstatus);
        showMsg("扫码错误请重试!");
        break;
    }
   
  };
  var fail = function () {
    
  };
  var complete = function () {
  };
  wx.request({
    url: url,
    data: data,
    header: { "Content-Type": "application/x-www-form-urlencoded" },
    method: 'POST',
    success: successmethod,
    fail: fail,
    complete: complete
  })
}
//用户状态判断
function statusJudge(){
  var bizinfo = wx.getStorageSync('bizinfo');
  var that = this;
  if (bizinfo.retcode != '103') {
    switch (bizinfo.bizStatus) {
      case "1003": {//可用状态
        
        break;
      }
      case "1004": {
        showMsg('您已租车');
        break;
      }
      case "1005": {
        // showMsg('您有未支付订单');
        wx.navigateTo({
          url: '../order/order'
        })
        break;
      }
      case "1001": {
        // showMsg('未开通租车业务');
        wx.navigateTo({
          url:'../deposit/deposit'
        })
        break;
      }
      case "1002": {
        // showMsg('未开通租车业务');
        wx.navigateTo({
          url: '../deposit/deposit'
        })
        break;
      }
      case "1006": {
        // showMsg('未开通租车业务');
        wx.navigateTo({
          url: '../deposit/deposit'
        })
        break;
      }
      case "108": {
        showMsg('一分钟内不能再租车');
        break;
      }
      default:
        {
          showMsg('操作失败请重试...');
          break;
        }
    }
    if (bizinfo.bizStatus == "1003"){
      return true;
    }else{
      return false;
    }
  } else {
    var backurl = '../login/login';
    wx.navigateTo({
      url: backurl
    })
    return false;
  }
}
//消息提示
function showMsg(message){
  // wx.hideLoading();
  // setTimeout(function(){
    isConnectedDeviced = true;
    wx.showToast({
      title: message,
      // mask:true,
    })
    wx.closeBluetoothAdapter({
      success: function (res) {
        console.log('关闭蓝牙模块成功');
      },
    })
  // },100)
  
}
//获取系统信息
function togetSysInfo() {
  var version;
  wx.getSystemInfo({
    success: function (res) {
      version = res.platform
    },
  })
  return version;
}

module.exports = {
  scan_code: scan_code,//扫码租车
  lockBike: lockBike,//关锁
  temporaryParking: temporaryParking,//临时停车
  continueBike: continueBike,//继续骑行
}
