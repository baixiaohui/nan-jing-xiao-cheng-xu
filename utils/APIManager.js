var model = require('/Model.js')
var urlManager = require('/URLManager.js')
var urlObject = urlManager.getUrlObject();
var uTil = require('/util.js');

/// 获取bizinfo
var getBizInfo = function (callback) {
  
  var url = urlObject.bizinfoUrl;
  var modelresult = function (res) {
    var result = new model.BizInfo(res);
    callback(result);
  }
  var data = {
    sessionId_: wx.getStorageSync("sessionId_")
  }
  uTil.requestMethodx(url, data, modelresult, "获取bizinfo");
}
///获取sessionID
var getSessionId = function (callback) {
  var url = urlObject.sessionUrl;
  var modelresult = function (res) {
    var result = new model.SessionID(res);
    wx.setStorageSync("sessionId_", result.sessionId_);
    callback();
  }
  wx.login({
    success: function (loginCode) {
      var data = {
        appid_mp: urlObject.appid,
        secret: urlObject.sercret,
        js_code: loginCode.code
      };
      uTil.requestMethodx(url, data, modelresult, "获取sessionID");
    }
  });
 
  
}
///获取用户openID
var getOpenId = function (callback) {
  wx.login({
    success: function (loginCode) {
      var url = urlObject.wxOpenIdUrl;
      var data = {
        appid: 'wxaa23aa1f2f62b4b7',
        secret: 'bff1d778b9906891cfe28373307bcc3d',
        js_code: loginCode.code
      }
      var requestSuccess = function (res) {
        
        if (res.data.openid) {
          console.log("请求" + "openid" + "成功");
          wx.setStorageSync("openid", res.data.openid);
          callback(res.data.openid);
        }else{
          console.log("请求" + "openid" + "错误" );
        }
      };
      var requestFail = function () {
        console.log("请求" + "openid" + "失败");
      };
      var requestCompelete = function () {
        console.log("请求" + "openid" + "结束");
      };
      uTil.netWorking(url, "POST", data, requestSuccess, requestFail, requestCompelete);

    }
  })
}

/// 获取附近站点
var getNearSation = function (lon, lat, stationType, callback) {
  var url = urlObject.nearStationUrl;
  var modelresult = function (res) {
    var result = new model.NearStation(res);
    callback(result);
  }
  var data = {
    sessionId_: wx.getStorageSync("sessionId_"),
    lon: lon,
    lat: lat,
    len: "1000",
    stationType: stationType,
  }
  uTil.requestMethodx(url, data, modelresult, "获取附近站点");
}
/// 获取无桩附近站点 含无桩搜索
var getNearStationWithoutStake = function (lon, lat, keyword, callback){
  var url = urlObject.unHaveStakeRenUrl;
  var modelresult = function (res) {
    var result = new model.NearStation(res);
    callback(result);
  }
  var data = {
    coordinate: String(lon) + "," + String(lat),
      keyword: keyword,
      coordtype:"1",
      range: "1000",
      sessionId_: wx.getStorageSync("sessionId_")
  }
  uTil.requestMethodx(url, data, modelresult, "获取无桩附近站点");
}
///搜索
var searchStions = function (keword, callback) {
  var url = urlObject.searchUrl;
  var modelresult = function (res) {
    var result = new model.SearchData(res);
    callback(result);
  }
  var data = {
    sessionId_: wx.getStorageSync("sessionId_"),
    name: keword,
  }
  uTil.requestMethodx(url, data, modelresult, "搜索");
}
///获取验证码   /*** 获取验证码 返回数据结构线下和线上不同 */
var getVcode = function (phone, callback,failcallback) {
  var url = urlObject.vcodeUrl;
  var modelresult = function (res) {
    console.log("返回结果验证码：：：：");
    console.log(res);
    // var result = new model.VCode(res); 叮嗒线下版本
   var result = new model.VCode(res.data);
    callback(result);
  }
  var data = {
    phone: phone,
    sessionId_: wx.getStorageSync('sessionId_')
  }
  var complete= function(){

  }
  uTil.netWorking(url, "POST", data, modelresult, failcallback, complete);
}

// ///登录
// var login = function (phone, vcode, callback) {
//   var url = urlObject.vcodeLoginUrl;
//   var modelresult = function (res) {
//     console.log("登录返回结果");
//     console.log(res);
//     var result = new model.LoginData(res);
//     console.log(result);
//     callback(result);
//   }
//   var data = {
//     sessionId_: wx.getStorageSync('sessionId_'),
//     userName: phone,
//     vcode: vcode
//   }
//   uTil.requestMethodx(url, data, modelresult, "登录");
// }
///密码登录
var login = function (phone, password, callback) {
  var url = urlObject.vcodeLoginUrl;
  var modelresult = function (res) {
    console.log("密码登录返回结果");
    console.log(res);
    var result = new model.LoginData(res);
    console.log(result);
    callback(result);
  }
  var data = {
    sessionId_: wx.getStorageSync('sessionId_'),
    phone: phone,
    password: password
  }
  console.log(data);
  uTil.requestMethodx(url, data, modelresult, "密码登录");
}
///注册或忘记密码
var register = function (phone, vcode, password, callback) {
  var url = urlObject.vcodeLoginUrl;
  var modelresult = function (res) {
    console.log("注册或忘记密码返回结果");
    console.log(res);
    var result = new model.RegisterData(res);
    console.log(result);
    callback(result);
  }
  var data = {
    sessionId_: wx.getStorageSync('sessionId_'),
    phone: phone,
    vcode: vcode,
    password: password
  }
  uTil.requestMethodx(url, data, modelresult, "注册或忘记密码");
}

///是否注册
var isRegisterAct = function (phone, callback) {
  var url = urlObject.isRegisterUrl;
  var modelresult = function (res) {
    console.log("是否注册返回结果");
    console.log(res);
    var result = new model.isRegisterActData(res);
    console.log(result);
    callback(result);
  }
  var data = {
    sessionId_: wx.getStorageSync('sessionId_'),
    phone: phone
  }
  uTil.requestMethodx(url, data, modelresult, "是否注册");
}
///退出登录
var loginOut = function (callback) {
  var url = urlObject.loginOutUrl;
  var modelresult = function (res) {
    var result = new model.LoginOut(res);
    callback(result);
  }

  var data = {
    sessionId_: wx.getStorageSync('sessionId_'),
  }
  uTil.requestMethodx(url, data, modelresult, "退出登录");
}
///获取城市信息
var getCityInfo = function (callback) {
  var url = urlObject.cityInfoUrl;
  var modelresult = function (res) {
    var result = new model.CityInfo(res);
    wx.setStorageSync("cityinfo", result)
    callback(result);
  }
  var data = {
    sessionId_: wx.getStorageSync('sessionId_'),
  }
  uTil.requestMethodx(url, data, modelresult, "城市信息");
}
///获取站点信息
var getStation = function(stationnumber,stationtype,callback){
  var url = urlObject.stationInfUrl;
  var modelresult = function (res) {
    var result = new model.StationInfo(res);
    callback(result);
  }
  var data = {
    sessionId_: wx.getStorageSync('sessionId_'),
    stationId: stationnumber,
    stationtype: stationtype
  }
  uTil.requestMethodx(url, data, modelresult, "站点信息");
}
///有桩租车请求
var requesthasStakeRentBike = function (siteNum, parkNum, bizType, callback) {
  var url = urlObject.hasStakeRentBikeUrl;
  var modelresult = function (res) {
    var result = new model.HasStakeRentBike(res);
    callback(result);
  }
  var data = {
    sessionId_: wx.getStorageSync('sessionId_'),
    siteNum: siteNum,
    parkNum: parkNum,
    bizType: bizType
  }
  uTil.requestMethodx(url, data, modelresult, "租车");
}
//获取支付凭证
var getCharge = function (orderId, channel, callback) {
  var url = urlObject.getChargeUrl;
  var modelresult = function (res) {
    var result = new model.GetCharge(res);
    callback(result);
  }
  var data = {
    sessionId_: wx.getStorageSync('sessionId_'),
    orderId: orderId,
    channel: channel
  }
  uTil.requestMethodx(url, data, callback, "创建支付凭证");
}
//获取行程记录
var getRecord = function (retcount, callback) {
  var url = urlObject.getRecordUrl;
  var modelresult = function (res) {
    var result = new model.TripRecords(res);
    callback(result);
  }
  var data = {
    sessionId_: wx.getStorageSync('sessionId_'),
    retcount: retcount
  }
  uTil.requestMethodx(url, data, modelresult, "获取行程记录");
}
///注销租车
var getDisableTrade = function (bizType, callback) {
  var url = urlObject.disableTradeUrl;
  var modelresult = function (res) {
    var result = new model.DisableTrade(res);
    callback(result);
  }
  var data = {
    sessionId_: wx.getStorageSync('sessionId_'),
    bizType: bizType
  }
  uTil.requestMethodx(url, data, modelresult, "注销租车");
}
/// 获取订单信息
var getordersAction = function (ordertype, callback) {
  var url = urlObject.getordersUrl;
  var modelresult = function (res) {
    var result = new model.Getorders(res);
    callback(result);
  }
  var data = {
    ordertype: ordertype,
    sessionId_: wx.getStorageSync('sessionId_')
  }
  uTil.requestMethodx(url, data, modelresult, " 获取订单信息");
}
//// 开通租车业务
var enableTradeAction = function (bizType, callback) {
  var url = urlObject.enableTradeUrl;
  var modelresult = function (res) {
    var result = new model.EnableTrade(res);
    callback(result);
  }
  var data = {
    bizType: bizType,
    sessionId_: wx.getStorageSync('sessionId_')
  }
  uTil.requestMethodx(url, data, modelresult, " 开通租车业务");
}
///检查订单状态
var checkOrderAction = function (orderId, callback) {
  var url = urlObject.checkOrderStatusUrl;
  var modelresult = function (res) {
    var result = new model.CheckOrder(res);
    callback(result);
  }
  var data = {
    orderId: orderId,
    sessionId_: wx.getStorageSync('sessionId_')
  }
  uTil.requestMethodx(url, data, modelresult, " 检查订单状态");
}
module.exports = {
  getStation: getStation,
  getSessionId: getSessionId,
  getOpenId: getOpenId,
  getBizInfo: getBizInfo,
  getCityInfo: getCityInfo,

  getOpenId: getOpenId,
  getNearSation: getNearSation,
  searchStions: searchStions,
  getVcode: getVcode,
  login: login,
  loginOut: loginOut,
  requesthasStakeRentBike: requesthasStakeRentBike,
  getCharge: getCharge,
  getRecord: getRecord,
  getDisableTrade: getDisableTrade,
  getordersAction: getordersAction,
  enableTradeAction: enableTradeAction,
  checkOrderAction: checkOrderAction,
  getNearStationWithoutStake: getNearStationWithoutStake,

  register: register,
  isRegisterAct: isRegisterAct
}