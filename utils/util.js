function formatTime(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()
  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

function json2Form(json) {
  var str = [];
  for (var p in json) {
    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(json[p]));
  }
  return str.join("&");
} 
//网络请求函数
var netWorking =function (url, method, data, successmethod, failmethod, compelete) {
  wx.request({
    url: url,
    data: data,
    header: { "Content-Type": "application/x-www-form-urlencoded" },
    method: method,
    success: successmethod,
    fail: failmethod,
    complete: compelete
  })
}

var requestMethodx = function (url, data, callback, message) {
  var that =this;
  var requestSuccess = function (res) {
    console.log("请求" + message + "成功");
    console.log(res);
    if (res.data.retcode) {
      callback(res.data);
    } else {
      console.log("请求" + message + "错误");
    }
  };
  var requestFail = function (res) {

    that.showTips(res.errMsg, "fail", "../../img/error@3x.png", 1, null);
    console.log("请求" + message + "失败");
  };
  var requestCompelete = function () {
 
    console.log("请求" + message + "结束");
  };
  this.netWorking(url, "POST", data, requestSuccess, requestFail, requestCompelete);
}
//请求bizinfo

//检测订单
var checkOrder = function (orderId) { 
  var url = app.data.urlObject.checkOrderUrl;
    var data = {
      sessionId_: wx.getStorageSync('sessionId_'),
      orderid: orderId
    };
    var success = function (res) {
      wx.setStorageSync('bizStatus', res.bizStatus);
    };
    var fail = function () {
  
      console.log("请求bizinf失败");
    };
    var compelete = function () {
      console.log("请求bizinfo完成");
    };
    this.netWorking(url, "POST", data, success, fail, compelete);
}

function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}
function showMsg(_title, backurl) {
  wx.showToast({
    title: _title,
    icon: 'loading',
    duration: 1000,
  });
  setTimeout(function () {
    wx.navigateBack({
      delta: 1, // 回退前 delta(默认为1) 页面
      success: function (res) {},
      fail: function () {},
      complete: function () {}
    })
  }, 500)
}
var showTips= function(title,icon,image,duration,mask,success,fail){
  wx.showToast({
    title: title,
    icon:icon,
    image:image,
    duration: duration*1000,
    mask:mask,
    success: success,
    fail:fail
  })
}
var showLoadingDD = function (title, mask,success,faile,compelete){
  wx.showLoading({
    title: title,
    mask:mask,
    success:success,
    fail:faile,
    complete: compelete
  })
}
var stringreplace = function(startindex,endindex,fromstring,tostring){
  return fromstring.substring(0, 3)+"****"+fromstring.substring(7,11);
}
module.exports = {
  formatTime: formatTime,
  netWorking: netWorking,
  showMsg: showMsg,
  checkOrder: checkOrder,
  requestMethodx: requestMethodx,
  showTips: showTips,
  showLoadingDD: showLoadingDD,
  stringreplace: stringreplace
}


