/***
 * 数据模型
 */

///bizinfo模型
class BizInfo {
  constructor(param) {
    this.retcode = param["retcode"];
    this.retmsg = param["retmsgak"];
    this.systime = param["systime"];
    this.userid = param["userid"];
    this.phonenum = param["phonenum"];
    this.bizStatus = param["bizStatus"];
    this.bizExtra = param["bizExtra"];
  }
}
/**
 * //sessionid
 */

class SessionID {
  constructor(param) {
    this.sessionId_ = param["sessionId_"];
    this.retcode = param["retcode"];
    this.retmsg = param["retmsg"];
  }
}
/**
 * openID
 */
class OpenID {
  constructor(param) {
    this.openId = param["openid"];
  }
}
/**
 * 附近站点
 */
class NearStation{
  constructor(param){
    this.retmsg = param["retmsg"];
    this.retcode = param["retcode"];
    this.datacount = param["datacount"];
    this.data = param["data"];

  }
}
/**
 * 搜索
 */
class SearchData{
  constructor(param) {
    this.retmsg = param["retmsg"];
    this.retcode = param["retcode"];
    this.datacount = param["datacount"];
    this.data = param["data"];
  }
}
/**
 * 验证码
 */
class VCode{
  constructor(param) {
    this.retmsg = param["retmsg"];
    this.retcode = param["retcode"];
  }
}
/***
 * 登录
 */
// class LoginData{
//   constructor(param) {
//     this.retmsg = param["retmsg"];
//     this.retcode = param["retcode"];
//     this.userId =param["userId"];
//     this.phoneNO = param["phoneNO"];
//     this.avatarURL = param["avatarURL"];
//     this.userToken = param["userToken"];
//     this.exchangeKey = param["exchangeKey"];
//     this.accessToken = param["accessToken"];
//   }
// }
/***
 * 密码登录
 */
class LoginData {
  constructor(param) {
    this.retmsg = param["retmsg"];
    this.retcode = param["retcode"];
    this.userId = param["userId"];
    this.phoneNO = param["phoneNO"];
    this.avatarURL = param["avatarURL"];
    this.userToken = param["userToken"];
    this.exchangeKey = param["exchangeKey"];
    this.accessToken = param["accessToken"];
  }
}
/***
 * 是否注册
 */
class isRegisterActData {
  constructor(param) {
    this.retmsg = param["retmsg"];
    this.retcode = param["retcode"];
    this.isRegister = param["isRegister"];
  }
}
/***
 * 注册或者忘记密码
 */
class RegisterData {
  constructor(param) {
    this.retmsg = param["retmsg"];
    this.retcode = param["retcode"];
    this.userId = param["userId"];
    this.phoneNO = param["phoneNO"];
    this.avatarURL = param["avatarURL"];
    this.userToken = param["userToken"];
    this.exchangeKey = param["exchangeKey"];
    this.accessToken = param["accessToken"];

  }
}
/***
 * 退出登录
 */
class LoginOut{
  constructor(param) {
    this.retmsg = param["retmsg"];
    this.retcode = param["retcode"];
  }
}
/**
 * 城市信息
 */
class CityInfo{
  constructor(param) {
    this.retmsg = param["retmsg"];
    this.retcode = param["retcode"];
    this.openmoney = param["openmoney"];
    this.cityname = param["cityname"];
    this.carriername = param["carriername"];
    this.data = param["data"];
  }
}
/***
 * 行程记录
 */
class TripRecords{
  constructor(param) {
    this.retmsg = param["retmsg"];
    this.retcode = param["retcode"];
    this.datacount = param["datacount"];
    this.data = param["data"];
  }
}
/**
 * 单个站点信息
 */
class StationInfo{
  constructor(param) {
    this.retmsg = param["retmsg"];
    this.retcode = param["retcode"];
    this.number = param["number"];
    this.name = param["name"];
    this.lon = param["lon"];
    this.lat = param["lat"];
    this.address = param["address"];
    this.status = param["status"];
    this.updatetime = param["updatetime"];
    this.totalcount = param["totalcount"];
    this.rentcount = param["rentcount"];
    this.restorecount = param["restorecount"];
  }
}
/***
 * 注销租车
 */
class DisableTrade {
  constructor(param) {
    this.retmsg = param["retmsg"];
    this.retcode = param["retcode"];
    this.bizstatus = param["bizstatus"];
  }
}

/***
 * 获取订单信息
 */
class Getorders {
  constructor(param) {
    this.retmsg = param["retmsg"];
    this.retcode = param["retcode"];
    this.datacount = param["datacount"];
    this.data = param["data"];

  }

}
/***
 * 检测订单状态
 */
class EnableTrade {
  constructor(param) {
    this.retmsg = param["retmsg"];
    this.retcode = param["retcode"];
    this.bizstatus = param["bizstatus"];
    this.orderid = param["orderid"];
    this.orderno = param["orderno"];
  }
}
/***
 * 获取支付凭证
 */
class GetCharge {
  constructor(param) {
    this.retmsg = param["retmsg"];
    this.retcode = param["retcode"];
    this.chargestatus = param["chargestatus"];
    this.chargedata = param["chargedata"];

  }
}
/***
 * 获取保证金状态
 */
class CheckOrder {
  constructor(param) {
    this.retmsg = param["retmsg"];
    this.retcode = param["retcode"];
    this.orderstatus = param["orderstatus"];
  }
}

module.exports = {
  BizInfo: BizInfo,
  SessionID: SessionID,
  TripRecords: TripRecords,
  CityInfo: CityInfo,
  LoginOut: LoginOut,
  LoginData: LoginData,
  VCode: VCode,
  SearchData: SearchData,
  NearStation: NearStation,
  OpenID: OpenID,
  GetCharge: GetCharge,
  DisableTrade: DisableTrade,
  Getorders: Getorders,
  EnableTrade: EnableTrade,
  CheckOrder: CheckOrder,
  StationInfo: StationInfo,
  isRegisterActData: isRegisterActData,
  RegisterData: RegisterData
}