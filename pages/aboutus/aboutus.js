//myCenter.js
var app = getApp();

Page({
  data: {
    userInfo: {},
    title: "叮嗒一声，伴你出行",
    waysArr: [
      {
        id: "0",
        src: "../../img/icon_phone0.png",
        keywords: "400-9696-556",
        curstyle: "width:51rpx;height: 50rpx;padding:25rpx 0;"
      },
      {
        id: "1",
        src: "../../img/icon_wechat0.png",
        keywords: "叮嗒出行",
        curstyle: "width:59rpx;height:48rpx;padding:26rpx 0;"
      },
      {
        id: "2",
        src: "../../img/icon_sina0.png",
        keywords: "@叮嗒出行",
        curstyle: "width:60rpx;height:48rpx;padding:26rpx 0;"
      },
      {
        id: "3",
        src: "../../img/icon_qq0.png",
        keywords: "414078575",
        curstyle: "width:57rpx;height:49rpx;padding:26rpx 0;"
      }
    ],
    curstyle: null,
    company: "杭州金通公共自行车科技股份有限公司"
  },
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh()
  },
  onLoad: function (backPage) {
    console.log(backPage);
    //调用应用实例的方法获取全局数据
    var that = this;
    app.getUserInfo(function (userInfo) {
      //更新数据
      that.setData({
        userInfo: userInfo
      })
    });
  },
  call: function (e) {
    var _id = e.currentTarget.dataset.id;
    console.log(_id);
    if (_id == 0) {
      wx.makePhoneCall({
        phoneNumber: '400-9696-556'
      })
    }

  },
  onReady: function () {
    // 页面渲染完成 
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  }
})
