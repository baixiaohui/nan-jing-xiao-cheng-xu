// pages/search/search.js
var order = ['red', 'yellow', 'blue', 'green', 'red'];
Page({
  /**
   * 页面的初始数据
   */
  data: {
    specialArr: null,
    title: null,
    array: [
      {
        _code: "001",
        _title: "如何注册、开通租车功能？",
        akk: "关注“智骑江北”微信公众号，进入扫码租车页面，点击页面右下角的头像进入个人中心，输入个人手机号码注册账户，点开“保证金”选项充值200元押金即可开始扫码租车。"
      },
      {
        _code: "002",
        _title: "如何租车？",
        _number: "8",
        akk:
        [
        { _number: "8", text: "1）	锁桩租车：进入“智骑江北”微信公众号，点击“扫码租车”按钮扫描锁桩上的二维码，听到租车成功提示，请于30秒内将车取走。" },
        { _number: "8", text: "2）	无桩租车：打开手机蓝牙，进入“智骑江北”微信小程序，点击“扫码租车按钮”，扫描车辆电子锁上的二维码，电子锁开启，手机扫码界面提示“开锁成功”。" },
        ]
      },
      {
        _code: "003",
        _title: "如何还车？",
        _number: "8",
        akk: [
          { _number: "8", text: "(1)锁桩还车：直接将自行车推入锁桩锁牢，锁桩提示“还车成功”提醒，收到微信还车信息即为还车成功。" },
          { _number: "8", text: "(2)无桩还车：在电子驿站范围内打开手机蓝牙，进入扫码租车界面，点击右上角的“还车”按钮，“叮”提示后，手机扫码界面提示“请手工关锁”，请手工把锁锁上，手机上会收到还车信息即表示还车成功。" },
        ]
      },
      {
        _code: "004",
        _title: "超时费怎么支付？",
        akk: "点击扫码租车页面右下角的头像标志进入个人中心，进入“订单详情”选项，选择需支付费用即可完成支付。"
      },
      // {
      //   _code: "005",
      //   _title: "如何导航？",
      //   akk: "点击首页地图中央的【闪电找车】按钮，APP会自动推荐离你最近的站点，再点击弹出的【导航按钮】即可开始导航。你也可以手动点击首页上的<站点图标>进行导航。"
      // },
      {
        _code: "101",
        _title: "一个账号能租几辆车？",
        akk: "一个手机账号一次只能租一辆车。"
      },
      {
        _code: "102",
        _title: "租车时间，收费标准？",
        _number: "8",
        akk: [
          { _number: "8", text: "每次租车2小时内免费，2小时以上将收取租车费用。" },
          { _number: "8", text: "（1）120分钟以内：免费租车；" },
          { _number: "8", text: "（2）120分钟-180分钟（含）：1元/小时；" },
          { _number: "8", text: "（3）超过180分钟，每小时3元计费（不足1小时以1小时计）。" },
          { _number: "8", text: "【使用扫码租车时，如果您需要骑行2个小时以上，可在任意站点归还自行车后再继续租用，系统将重新计时】" },
        ]
      },
      {
        _code: "103",
        _title: "站点租车成功后拉不出来怎么办？",
        _number: "8",
        akk: [
          { _number: "8", text: "(1) 租还车异常时请及时联系客服：400-828-1968" },
          { _number: "8", text: "(2) 或者通过微信公众号反馈提交您的注册手机号码和问题详情，我们会在第一时间给您核实处理。" },
        ]
      },
      // {
      //   _code: "104",
      //   _title: "还车成功，仍然在倒计时？",
      //   _number: "8",
      //   akk: [
      //     { _number: "8", text: "手机APP行程显示未完成:" },
      //     { _number: "8", text: "1）请联系当地客服号码或4009696556。" },
      //     { _number: "8", text: "2）通过APP意见反馈提交您的注册手机号和还车站点号（车桩二维码上方数字或站点名称），我们会在第一时间给您核实处理。" }
      //   ] 
      // },
      // {
      //   _code: "105",
      //   _title: "租不了车怎么办？",
      //   akk: "若提示租车失败请换个锁桩进行租车，在换了好几个锁桩都是租车失败的情况下，有可能是该站点网络异常导致无法租车，建议换个站点尝试租车，给您带来不便非常抱歉。"
      // },
      {
        _code: "201",
        _title: "如何退还保证金？",
        akk: "点击扫码租车页面右下角的头像标志进入个人中心，进入“保证金”选择“注销租车”选项，退还保证金。保证金将会在7个工作日之内原路退还。"
      },
      // {
      //   _code: "202",
      //   _title: "申请退还保证金后，多久退回？",
      //   akk: "成功申请退还后将在7个工作日内原路退回到支付账户中。"
      // },
      // {
      //   _code: "203",
      //   _title: "已缴保证金，但未开通租车怎么办？",
      //   _number: "8",
      //   akk: [
      //     { _number: "8", text: "(1) 请加叮嗒出行全国粉丝群：414078575" },
      //     { _number: "8", text: "(2) 请联系当地客服号码或全国客服电话4009696556。" }
      //   ] 
      // },
    ]

  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    console.log(options.id);
    console.log(options.num);

    var alldata = that.data.array;
    for (var i = 0; i < alldata.length; i++) {
      if (alldata[i]._code == options.num) {
        //  var add = JSON.stringify(alldata[i].akk);
        that.setData({
          curwords: alldata[i],
          specialArr: alldata[i].akk,
          title: alldata[i]._title
        });
        console.log(that.data.specialArr[0].text);
      }
    }
    var actitle = that.data.title;
    wx.setNavigationBarTitle({
      title: actitle,
      success: function () {
        title: actitle,
          console.log("当前微信小程序的页面标题为\"" + actitle + "\"");
      },
      complete: function () {
        console.log("动态修改微信小程序的页面标题-complete");
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  }
})