// pages/useInstruciton/useInstruciton.js
var imageUtil = require('../../utils/imgscole.js');  
Page({

  /**
   * 页面的初始数据
   */
  data: {
  infoArr:[
    {
      num:"01",
      title:"登录",
      words:"打开叮嗒出行APP，首页点击左上角的“菜单”或点击“扫码租车”按钮，就会进入登录页面，输入正确的手机号码进行验证即可登录。",
      imgsrc:"../../img/login.jpg"
    },
    {
      num: "02",
      title: "手机扫码开锁",
      words: "打开叮嗒出行APP，点击扫码按钮，扫描车桩上的二维码，租车提示音响起即租车成功。",
      imgsrc: "../../img/has1.jpg"
    },
    {
      num: "03",
      title: "拉车出桩",
      words: "租车成功后将车从车桩上拉出，提示租车成功。即可享受骑行乐趣。",
      imgsrc: "../../img/has2.jpg"
    },
    {
      num: "04",
      title: "推车入桩还车",
      words: "要还车时，找到公共自行车停车点，将车推入车桩，还车成功的提示音响起即表示还车成功。",
      imgsrc: "../../img/has3.jpg"
    }
  ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },
  imageLoad:function(e){
    var imageSize = imageUtil.imageUtil(e);
    this.setData({
      imagewidth: imageSize.imageWidth*0.9,
      imageheight: imageSize.imageHeight*0.9
    }) 
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  }
})