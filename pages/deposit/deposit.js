
var app = getApp();
var uTil = require('../../utils/util.js');
var pingpp = require('../../utils/pingpp.js');
var apiM = require('../../utils/APIManager.js');
var noMoreClick=false;
Page({
  data: {
    userInfo: {},
    deposit:"--",
    keywords:"",
    payrecord:[],
    bizFlag: false,//充值为true,注销租车为false
    blanksrc: "",
    showPayRecord:false
  },
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh()
  },
  onLoad: function (backPage) {
 
  },
  //初始化 根据bizStatus渲染页面
  switchUserStatus: function () {
    var that = this;
    var bizStatus = wx.getStorageSync('bizinfo').bizStatus;
    switch (bizStatus) {
      case "1003": //正常状态
      case "1004": //租车中
      case "1005": {
        that.setData({
          keywords: "注销租车",
          bizFlag: false,
          deposit: Number(wx.getStorageSync('cityinfo').openmoney/100).toFixed(2)
        });
        that.getOrderActList();
      }
        break;
      case "1001":{
        that.setData({
          keywords: "充值",
          deposit: "0.00",
          bizFlag: true,
          blanksrc: "../../img/img_wallet@2x.png"
        }); 
      }
        break;
      case "1002": 
      case "1006": {
        that.setData({
          keywords: "充值",
          deposit: "0.00",
          bizFlag: true,
        }); 
        that.getOrderActList();
      }
        break;
    }
  },
//获取支付记录
getOrderActList:function(){
  var that = this;
  uTil.showLoadingDD("获取支付记录...",true,null,null);
  apiM.getordersAction(
    "2", function (res) {
    
      if (res.retcode == "0") {
        wx.hideLoading();
        if (res.data.length > 0) {
          that.setData({
            payrecord: res.data,
            showPayRecord: true
          })
        } else {
          that.setData({
            showPayRecord: false,
            blanksrc: "../../img/img_wallet@2x.png"
          })
        }
      } else {
        uTil.showTips("获取记录失败", "fail", "../../img/error@3x.png", 1, null);
      }
    });
},


  //点击按钮绑定事件（分注销租车和充值）
  quitOrPayBtn:function(){
    var that=this;
    var bizStatus = (wx.getStorageSync('bizinfo')).bizStatus;
    if (bizStatus == "1002") { //开通业务支付中
        that.jumpPage("../order/order");
    } else if (bizStatus == "1001" || bizStatus == "1006") { //新建       
        uTil.showLoadingDD("创建支付...", true, null, null);
        that.enableTradeAction();
    } else{ //注销租车
      wx.showModal({
        title: '温馨提示',
        content: '您确定要注销租车吗?',
        success: function (res) {
          if (res.confirm) {
            console.log('用户点击确定')
            uTil.showLoadingDD("注销中...", true, null, null);
            that.disableTrade();
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
          
      
        }
      })    
    }
  },

    //请求支付getCharge,调用pingpp
    getCharge: function (orderid) {
      var that = this;
      apiM.getCharge(
        orderid, 'wx_lite'
        , function (res) {
          wx.hideLoading();
          if (res.retcode == "0") {
            
            pingpp.createPayment(res.chargedata, function (result, err) {
              if (result == "success") {
                uTil.showTips("开通租车成功", "success", null, 1, null);
                that.setData({
                  deposit: wx.getStorageSync('cityinfo').openmoney/100,
                  keywords: "注销租车",
                  bizFlag:false
                });
              
                var bizinfo = wx.getStorageSync('bizinfo');
                bizinfo.bizStatus = "1003";
                wx.setStorageSync('bizinfo', bizinfo);
              } else if (result == "fail") { 
               //wx.hideLoading();
                // that.showMessage("支付失败")
                that.setData({
                  bizFlag: true
                });  
                that.refreshStatus(orderid);       
              } else if (result == "cancel") {
                uTil.showTips("您已取消支付!", "fail", "../../img/error@3x.png", 1, null);
              } 
            });
          } else {
         
            uTil.showTips("获取支付信息失败", "fail", "../../img/error@3x.png", 1, null);
          }
        });
    },
    refreshStatus: function (orderid) {
      var that = this;
      that.checkOrderEvent(orderid);
    },

    //检查订单状态
    checkOrderEvent: function (orderId) {
      var that = this;
      apiM.getordersAction(
        "2", function (res) {
          wx.hideLoading();
          var curOrdeid;
          for (var i = 0; i < res.data.length; i++) {
            if (res.data[i].orderstatus == "1" || res.data[i].orderstatus == "2") {
              curOrdeid = res.data[i].orderid;
              break;
            }
          }
          apiM.checkOrderAction(
            curOrdeid, function (res) {
              if (res.retcode == "0") {
                apiM.getBizInfo(function (bizinfo) {
                  wx.setStorageSync("bizinfo", bizinfo);
                  that.switchUserStatus();
                })
              }
            });
        });
      
    },
  
    //开通租车业务
    enableTradeAction: function () {
      var that = this;
      apiM.enableTradeAction(
        "1", function (res) {
          if (res.retcode == "0") {
            var bizinfo = wx.getStorageSync('bizinfo');
            bizinfo.bizStatus = "1002";
            wx.setStorageSync('bizinfo', bizinfo);
            that.getCharge(res.orderid);
          } else {
          
            uTil.showTips("创建支付失败", "fail", "../../img/error@3x.png", 1, null);
          }
        });
    },
    //注销租车
    disableTrade: function () {
      var that = this;
      apiM.getDisableTrade(
        "1", function (res) {
          if (res.bizstatus == "1006") {
          
            uTil.showTips(res.retmsg, "success", null, 1, null);
                var bizinfo = wx.getStorageSync('bizinfo');
                bizinfo.bizStatus = res.bizstatus;
                wx.setStorageSync('bizinfo', bizinfo);
                that.setData({
                  deposit: "0.00",
                  keywords: "充值"
                });

          }else{
          
            wx.showToast({
              title: res.retmsg,
              icon:"success",
              image: "../../img/error@3x.png",
              duration:1000,
              mask:true
            })
            //uTil.showTips(res.retmsg, "fail", "../../img/error@3x.png", 2.0, null);
          }
        });
    },

    /// 点击我已租车按钮绑定事件
    refreshStatusBtn:function(){
      var that=this;
      that.checkOrderEvent();
    },
    onReady:function(){},
    onShow:function(){
      
      this.switchUserStatus();
   
    },
    onHide:function(){},
    onUnload:function(){}, 
    //提示框
    showMessage: function (msg) {
      wx.showLoading({
        title: msg,
      });
      setTimeout(function () {
        wx.hideLoading()
      }, 1000)
    },
    // 跳转页面
    jumpPage:function(backpage){
      wx.navigateTo({
        url: backpage
      })
    }
})
