
/**
 * 连接设备。获取数据
 */
var apiM = require('../../utils/APIManager.js');
var operId = '';//上传数据所需参数
var deviceId = '';//设备编号(跟请求服务的deviceid不对应！！)
var name = '';//设备名称(请求服务对应的deviceid！！)
var citycode = '';
var serviceId = '';
var services = [];
var writeCharacter = '';
var notifyCharacter = '';
var result;
var renzheng = false;//是否走过认证方法
var openL = 1;//1，未进行开锁关锁操作。2，开锁指令发送完成。3，关锁指令发送完成，4关锁
var isClosed = '';//是否是关锁指令
var noOpenFunc = 1;//1,未接受过开锁指令，2，接受过开锁指令
var openCallbackFunc;
var closeCallBackFunc;
var systemversion;
function scanPage(getdeviceId, getname, getisClosed, getcitycode, blueRentBikeCallBike, resultBack) {
  operId = '';
  deviceId = '';
  name = '';
  citycode = '';
  serviceId = '';
  services = [];
  writeCharacter = '';
  notifyCharacter = '';
  result;
  renzheng = false;
  openL = 1;
  isClosed = '';
  noOpenFunc = 1;
  openCallbackFunc = null;
  closeCallBackFunc = null;
  console.log("onLoad");
  if (togetSysInfo()) {
    systemversion = togetSysInfo();
  }

  deviceId = getdeviceId;
  name = getname;
  citycode = getcitycode;
  isClosed = getisClosed;
  closeCallBackFunc = blueRentBikeCallBike;
  openCallbackFunc = blueRentBikeCallBike;
  serviceId = '6e400001-b5a3-f393-e0a9-e50e24dcca9e';
  notifyCharacter = '6e400003-b5a3-f393-e0a9-e50e24dcca9e';
  writeCharacter = '6e400002-b5a3-f393-e0a9-e50e24dcca9e';
  // if (systemversion == 'ios') {
    notifyCharacter = notifyCharacter.toUpperCase();
    writeCharacter = writeCharacter.toUpperCase();
    serviceId = serviceId.toUpperCase();
  // }

  wx.stopBluetoothDevicesDiscovery({
    success: function (res) {
      console.log("--====---stopBluetoothDevicesDiscovery--success-------======---");
    },
  })
  /**
   * 监听设备的连接状态
   */
  wx.onBLEConnectionStateChanged(function (res) {
    console.log(`device ${res.deviceId} state has changed, connected: ${res.connected}`)
    // resultBack({ 'success': 'false', 'title': '与蓝牙设备连接断开!' });
    // serviceId = '';
    if (res.connected == false) {
      wx.hideLoading();

    }


  })
  /**
   * 连接设备
   */

  wx.createBLEConnection({
    deviceId: deviceId,
    success: function (res) {
      // success
      console.log(res);
      console.log('createBLEConnectionres::::' + serviceId);
      // resultBack({ 'success': 'firstback', 'content': 'connectback','title':'' });
      wx.setStorageSync('connectSu_jc', 'connectSu_jc');
      /**
       * 连接成功，后开始获取设备的服务列表
       */
      // setTimeout(function () {
        wx.getBLEDeviceServices({
          deviceId: deviceId,
          success: function (res) {
            console.log('getBLEDeviceServices::::', res);
            /**
             * 根据服务获取特征 
             */
            // setTimeout(function () {
              wx.getBLEDeviceCharacteristics({
                deviceId: deviceId,
                serviceId: serviceId,
                success: function (res) {

                  console.log(res);
                  /**
                    * 回调获取 设备发过来的数据
                    */
                  /**
                                 * 顺序开发设备特征notifiy
                                 */
                  // setTimeout(function(){
                  // wx.notifyBLECharacteristicValueChanged({
                  //   deviceId: deviceId,
                  //   serviceId: serviceId,
                  //   characteristicId: notifyCharacter,
                  //   state: false,
                  //   success: function (res) {
                  //   }
                  // });
                  // },50);

                  setTimeout(function () {
                    wx.notifyBLECharacteristicValueChanged({
                      deviceId: deviceId,
                      serviceId: serviceId,
                      characteristicId: notifyCharacter,
                      state: true,
                      success: function (res) {
                        getSecret();
                        // success
                        console.log('notifyBLECharacteristicValueChanged success', res);
                        wx.onBLECharacteristicValueChange(function (res) {
                          var buffer = res.value
                          var dataView = new DataView(buffer)
                          var dataResult = []
                          console.log("拿到的数据")
                          console.log("dataView.byteLength", dataView.byteLength)
                          for (var i = 0; i < dataView.byteLength; i++) {
                            console.log(dataView.getUint8(i).toString(16))
                            dataResult.push(dataView.getUint8(i).toString(16))

                          }

                          result = dataResult;
                          //身份认证
                          if (renzheng == false) {
                            setTimeout(function () {
                              identifineQuery();
                            }, 10)

                            return;
                          }

                          //身份认证返回数据
                          if (renzheng == true && openL == 1) {
                            setTimeout(function () {
                              if (result.length == 3) {
                                failToast('身份认证失败', resultBack);

                              } else {

                                openLock(openCallbackFunc, resultBack);


                              }
                            }, 10)
                            return;

                          }
                          console.log('operId====' + operId);
                          //开锁返回数据
                          if (renzheng == true && openL == 2 && noOpenFunc == 1) {
                            noOpenFunc = 2;
                            console.log('开锁之后====' + dataResult);
                            
                            var datatype = '1';
                            if(isClosed == 'jixu'){
                              datatype = '6';
                            }
                            if (dataResult[5] == '51') {
                              
                              sendRentDataToService(name, citycode, operId, datatype, '1', function (res) {
                                console.log(res.data.reqStatus + "租车成功上传");
                                resultBack({ 'success': 'true', 'title': '开锁成功' });
                                apiM.getBizInfo(function (bizinfo) {
                                  wx.setStorageSync("bizinfo", bizinfo);
                                  
                                })

                              })

                            } else {
                              if (isClosed == 'jixu'){
                                  return;
                              }
                              sendRentDataToService(name, citycode, operId, '3', '2', function (res) {
                                console.log(res.data.reqStatus + "租车失败上传");


                                apiM.getBizInfo(function (bizinfo) {
                                  wx.setStorageSync("bizinfo", bizinfo);
                                })

                              })

                            }


                            return;
                          }
                          //关锁返回数据
                          if (renzheng == true && (isClosed == 'back' || isClosed == 'linshi')) {
                            var datatypez = '2';
                            if (isClosed == 'linshi') {
                              datatypez = '5';
                            }
                            console.log(openL + '关锁状态:' + dataResult[5]);
                            // setTimeout(function () {
                              if (openL == 3) {
                                // setTimeout(function () {
                                  if (dataResult[5] == 'a1') {
                                    sendRentDataToService(name, citycode, operId, datatypez, '2', function (res) {
                                      console.log(res.data.reqStatus + "还车上传成功");
                                      apiM.getBizInfo(function (bizinfo) {
                                        wx.setStorageSync("bizinfo", bizinfo);
                                        resultBack({ 'success': 'true', 'title': '关锁成功' });
                                        closeCallBackFunc({ 'rentType': '2', 'bizStatus': bizinfo.bizStatus });
                                        if (isClosed == 'back') {
                                          wx.removeStorageSync('blueDevice_id');
                                          wx.removeStorageSync('blueDevice_name');
                                        }
                                        

                                      })


                                    })


                                  } else {
                                    openL = 4;
                                    resultBack({ 'success': 'allback', 'title': '请手动关锁' });
                                  }
                                  return;
                                // }, 10)

                              } else if (openL == 4) {
                                //手动关锁返回
                                console.log('还车上传data=====' + dataResult);
                                setTimeout(function () {
                                  if (operId == null) {
                                    return;
                                  }
                                  if (dataResult[5] == 'a1') {
                                    sendRentDataToService(name, citycode, operId, datatypez, '2', function (res) {
                                      console.log(res.data.reqStatus + "还车上传成功");
                                      apiM.getBizInfo(function (bizinfo) {
                                        wx.setStorageSync("bizinfo", bizinfo);
                                        resultBack({ 'success': 'true', 'title': '关锁成功' });
                                        closeCallBackFunc({ 'rentType': '2', 'bizStatus': bizinfo.bizStatus });
                                        if (isClosed == 'back') {
                                          wx.removeStorageSync('blueDevice_id');
                                          wx.removeStorageSync('blueDevice_name');
                                        }

                                      })

                                    })
                                    return;
                                  } else {
                                    resultBack({ 'success': 'false', 'title': '还车失败请重试' });
                                    return;
                                  }
                                }, 10)



                                // wx.closeBluetoothAdapter({
                                //   success: function (res) {
                                //     console.log('断开连接!');
                                //   },
                                // })
                                wx.closeBLEConnection({
                                  deviceId: deviceId,
                                  success: function (res) { },
                                })

                                openL = 5;
                                return;
                              }
                            // }, 10);



                          }
                          if (openL != 3 && systemversion != 'ios') {
                            // wx.notifyBLECharacteristicValueChanged({
                            //   deviceId: deviceId,
                            //   serviceId: serviceId,
                            //   characteristicId: notifyCharacter,
                            //   state: false,
                            //   success: function (res) {
                            //   }
                            // });
                          }

                        });



                      },
                      fail: function (res) {
                        // fail
                        failToast('notify功能检测失败！', resultBack);
                        console.log('notifyBLECharacteristicValueChanged fail', res);
                      },
                      complete: function (res) {
                        // complete


                      }
                    })
                  }, 10)





                }, fail: function (res) {
                  failToast('获取蓝牙信息失败', resultBack);
                  console.log(res);
                }, complete: function (res) {

                  // complete
                }
              })
           

          }, complete: function (res) {

            // complete
          }
        })
      // }, 10)

    },
    fail: function (res) {
      failToast('连接设备失败', resultBack);
      // fail
    },
    complete: function (res) {
      console.log('连接完成');
      // complete
    }
  })

}

/**
 * 发送 数据到设备中 获取种子
 */
function getSecret() {
  let buffer = new ArrayBuffer(3)
  let dataView = new DataView(buffer)
  //写入通道指令 
  dataView.setUint8(0, 0xAA)    //这里也能写十进制数
  dataView.setUint8(1, 0x01)    //...
  dataView.setUint8(2, 0x18)


  console.log("发送的数据获取种子：")
  for (let i = 0; i < dataView.byteLength; i++) {
    console.log("0x" + dataView.getUint8(i).toString(16))
  }
  sendInfoToble(buffer);


}
/**
 * 发送 数据到设备中 身份认证
 */
function identifineQuery() {
  console.log("identifineQuery" + renzheng);
  if (renzheng == true) {
    return;
  }
  renzheng = true;
  if (result.length) {
    let bufferData = idAuth()
    console.log("发送的身份认证");
    sendInfoToble(bufferData.buffer)
    return;

  }



}
/**
 * 发送 数据到设备中 开锁
 */
function openLock(openResultCallback, failrentbike) {
  console.log("openLock-------" + openL);

  if (renzheng == true && openL == 1) {
    console.log('关锁操作返回的数据:' + result[7]);
    var bikeId = name;
    if (bikeId && (isClosed == 'back' || isClosed == 'linshi')) {
      var biketype;
      if (isClosed == 'back'){
        biketype = '32';
      }else{
        biketype = '35';
      }
      sendRentBikeRequest('0', name, citycode, biketype, function (res) {
        var resultForJudge = judgeBackBikeResultTojson(res);
        if (resultForJudge == 1) {
          openL = 3;
          closeLock();
        } else {
          openL = 5;
          failToast("关锁请求失败", failrentbike);
          // wx.closeBluetoothAdapter({
          //   success: function (res) {
          //     console.log('断开连接!');
          //   },
          // })
          wx.closeBLEConnection({
            deviceId: deviceId,
            success: function (res) { },
          })
        }

      }, failrentbike);

      return;
    }

    //开锁
    console.log('kais开锁操作:' + result);
    if (result[7] == '51') {
      failToast('此锁已开，请换车骑乘', failrentbike);

      // wx.closeBluetoothAdapter({
      //   success: function (res) {

      //   },
      // })
      wx.closeBLEConnection({
        deviceId: deviceId,
        success: function (res) { },
      })
      return;
    }
    if (isClosed == 'rent' || isClosed == 'jixu') {
      openL = 2;
      var biketype;
      if (isClosed == 'rent') {
        biketype = '31';
      } else {
        biketype = '36';
      }
      sendRentBikeRequest('0', name, citycode, biketype, function (res) {
        var resultForJudge = judgeRentBikeResultToJson(res,biketype);
        if (resultForJudge == 1) {

          openResultCallback({ 'rentType': '2', 'bizStatus': '1004' });
          let buffer = new ArrayBuffer(3)
          let dataView = new DataView(buffer)
          //写入通道指令 
          dataView.setUint8(0, 0xAA)    //这里也能写十进制数
          dataView.setUint8(1, 0x10)    //...
          dataView.setUint8(2, 0x18)


          console.log("发送的数据开锁：")
          for (let i = 0; i < dataView.byteLength; i++) {
            console.log("0x" + dataView.getUint8(i).toString(16))
          }
          sendInfoToble(buffer);
          wx.setStorageSync('blueDevice_id', deviceId);
          wx.setStorageSync('blueDevice_name', name);
        } else if (resultForJudge == 2) {

          failToast('开锁失败请重试', failrentbike);

          // wx.closeBluetoothAdapter({
          //   success: function (res) {
          //     console.log('断开连接!');
          //   },
          // })

          wx.closeBLEConnection({
            deviceId: deviceId,
            success: function (res) { },
          })
        } else if (resultForJudge == 3) {
          failToast('设备未入库', failrentbike);
          wx.closeBLEConnection({
            deviceId: deviceId,
            success: function (res) { },
          })
        }
      }, failrentbike);
    }

  }
}
/**
 * 发送 数据到设备中 关锁
 */
function closeLock() {

  let buffer = new ArrayBuffer(3)
  let dataView = new DataView(buffer)
  //写入通道指令 
  dataView.setUint8(0, 0xAA)    //这里也能写十进制数
  dataView.setUint8(1, 0x11)    //...
  dataView.setUint8(2, 0x18)

  console.log("发送的数据关锁：")
  for (let i = 0; i < dataView.byteLength; i++) {
    console.log("0x" + dataView.getUint8(i).toString(16))
  }
  sendInfoToble(buffer);

  return;

}
//指令发送
function sendInfoToble(buffer) {
  if (systemversion != 'ios') {
  //   wx.notifyBLECharacteristicValueChanged({
  //     deviceId: deviceId,
  //     serviceId: serviceId,
  //     characteristicId: notifyCharacter,
  //     state: true,
  //     success: function (res) {
  //     }
  //   });
    setTimeout(function () {
      wx.writeBLECharacteristicValue({
        deviceId: deviceId,
        serviceId: serviceId,
        characteristicId: writeCharacter,
        value: buffer,
        success: function (res) {
          // success
          console.log("success  指令发送成功");
          console.log(res);
        },
        fail: function (res) {
          // fail
          console.log(res);
        },
        complete: function (res) {
          // complete

        }
      })
    }, 200)
  } else {
    wx.writeBLECharacteristicValue({
      deviceId: deviceId,
      serviceId: serviceId,
      characteristicId: writeCharacter,
      value: buffer,
      success: function (res) {
        // success
        console.log("success  指令发送成功");
        console.log(res);
      },
      fail: function (res) {
        // fail
        console.log(res);
      },
      complete: function (res) {
        // complete

      }
    })
  }





}

function buf2hex(buffer) { // buffer is an ArrayBuffer
  return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
}
//种子加密
function idAuth() {
  // body...
  var mybyte = [0x43, 0x08, 0x53, 0x08, 0xA7, 0x08, 0xB3, 0x08, 0xBE, 0x08, 0x74, 0x0C, 0x7A, 0x0C, 0x82, 0x0C,
    0xA6, 0x0C, 0xC8, 0x0C, 0xFB, 0x0C, 0x25, 0x0D, 0x40, 0x0D, 0x4E, 0x0D, 0x6A, 0x0D, 0xE3, 0x0D,
    0x94, 0x0D, 0x64, 0x0E, 0xA1, 0x0E, 0xBD, 0x0E, 0xE2, 0x0E, 0xDE, 0xBF, 0xCD, 0xBF, 0x11, 0xE0,
    0xA4, 0x91, 0x8F, 0x10, 0x81, 0x16, 0xD5, 0x08];
  // var resulybyte = [0x55, 0x01, 0x38, 0x10, 0x16, 0x57, 0x9f];
  var resulybyte = result;

  mybyte.push(parseInt(resulybyte[3], 16));
  mybyte.push(parseInt(resulybyte[4], 16));
  mybyte.push(parseInt(resulybyte[5], 16));
  mybyte.push(parseInt(resulybyte[6], 16));

  mybyte.push(0x01);
  mybyte.push(0x02);
  mybyte.push(0x03);
  mybyte.push(0x04);

  var charmy = HashSha1(mybyte);
  // console.log(resulybyte+"charmy" + charmy + "//mybyte:" + mybyte);
  var mybyteSecond = new Uint8Array(11);

  //指令标识
  mybyteSecond[0] = 0xAA;
  mybyteSecond[1] = 0x02;
  mybyteSecond[2] = 0x58;//位数


  //App种子
  mybyteSecond[3] = 0x01;
  mybyteSecond[4] = 0x02;
  mybyteSecond[5] = 0x03;
  mybyteSecond[6] = 0x04;

  mybyteSecond[10] = (charmy % 256);
  charmy /= 256
  mybyteSecond[9] = (charmy % 256);
  charmy /= 256
  mybyteSecond[8] = (charmy % 256);
  charmy /= 256
  mybyteSecond[7] = (charmy % 256);
  // console.log("charmy" + mima);
  // console.log("mybyteSecond：" + mybyteSecond);
  return mybyteSecond;
}

//发送租车请求
function sendRentBikeRequest(requestType, namesend, citycodesend, bizType, callback, failrentbike) {
  console.log("租车请求++++++--======：");
  console.log(namesend);
  //租车请求
  wx.request({
    url: 'https://nj.dingdatech.com/nest-nre/rest/bikegw/business/bhtrequest',
    data: {
      serviceId: '43',
      requestType: requestType,
      deviceId: namesend,
      parkNum: '0',
      cityCode: citycodesend,
      bizType: bizType,//租车31，还车32
      sessionId_: wx.getStorageSync("sessionId_"),
    },
    header: { "Content-Type": "application/x-www-form-urlencoded" },
    method: 'POST',
    success: function (res) {
      console.log('租还车请求发送成功:');
      console.log(res);
      console.log(bizType);
      operId = res.data.operId;

      callback(res);
    },
    fail: function (res) {
      console.log('租还车请求发送失败:');
      console.log(res);
      if (bizType == '31') {
        failToast('租车请求失败', failrentbike);
      } else {
        failToast('还车请求失败', failrentbike);
      }

    },
    complete: function (res) {

    }
  });
}

//发送租车请求结果
function judgeRentBikeResultToJson(res,requestType) {
  console.log('发送租还请求');
  var resultR = (res.data.reqStatus);
  return judgeRentBikeResult(resultR, requestType);
}
//发送还车请求结果
function judgeBackBikeResultTojson(res) {
  var resultR = (res.data.reqStatus);
  return judgeBackBikeResult(resultR);
}
//租车请求返回结果判断
function judgeRentBikeResult(reqStatus, requestT) {

  if (reqStatus == '4000' || reqStatus == '4002') {
    if(requestT == '31'){
      return 1;
    }
    return 2;
    
  } else if (reqStatus == '4001') {
    return 2;
  } else if (reqStatus == '4003') {
    if (requestT == '36'){
      return 1;
    }
    return 2;
  } else if (reqStatus == '4102') {
    return 3;//设备未入库
  }
}
//还车请求返回结果判断
function judgeBackBikeResult(reqStatus) {
  if (reqStatus == '4002') {
    return 2;
  } else if (reqStatus == '4001' || reqStatus == '4003' || reqStatus == '4000') {

    return 1;
  }
}
//数据上送
function sendRentDataToService(nameto, citycodeto, operIdto, dataType, lockStatus, callback) {
  //租还车数据上送
  wx.request({
    url: 'https://nj.dingdatech.com/nest-nre/rest/bikegw/device/bhttrip',
    data: {
      serviceId: '43',
      deviceId: nameto,
      cityCode: citycodeto,
      sessionId_: wx.getStorageSync("sessionId_"),
      operId: operIdto,
      dataType: dataType,
      deviceType: '1',
      lockStatus: lockStatus,
      coordinate: wx.getStorageSync('lng') + ',' + wx.getStorageSync('lat'),
      deviceStakeId: wx.getStorageSync('deviceStakeId'),

    },
    header: { "Content-Type": "application/x-www-form-urlencoded" },
    method: 'POST',
    success: function (res) {
      console.log('数据上送发送成功:');
      console.log(res);
      callback(res);
      operId = null;
    },
    fail: function (res) {
      console.log('数据上送发送失败:');
      console.log(res);
      // failToast('还车失败请重试', failrentbike);
    },
    complete: function (res) {
      console.log('数据上送发送完成');

      // wx.closeBluetoothAdapter({
      //   success: function (res) {
      //     console.log('断开连接!');
      //   },
      // })
      wx.closeBLEConnection({
        deviceId: deviceId,
        success: function (res) { },
      })
      // 

    }
  });
}
//操作失败提示
function failToast(failTitle, failback) {
  // wx.closeBluetoothAdapter({
  //   success: function (res) {
  //     console.log('断开连接!');
  //   },
  // })
  wx.closeBLEConnection({
    deviceId: deviceId,
    success: function (res) { },
  })
  failback({ 'success': 'false', 'title': failTitle });


}
//获取系统信息
function togetSysInfo() {
  var version;
  wx.getSystemInfo({
    success: function (res) {
      version = res.platform
    },
  })
  console.log('version', version);
  return version;
}



//+++++++==========================================================




//以下为加密算法，勿动！！！
var KT1 = 0x5a827999;
var KT2 = 0x6ed9eba1;
var KT3 = 0x8f1bbcdc;
var KT4 = 0xca62c1d6;

var wTemp;
var A, B, C, D, E;
var w = new Array(80);
var m = new Array(16);
var cMac = new Array(20);

function S_LEFT(x, n) {
  // body...
  var wTmp;
  wTmp = zuoyi(x, n);
  for (var i = 0; i < 32 - n; i++) {
    x = parseInt(x / 2);
  }
  // console.log("xxxxxxxxxxxxxx\n"+x+"wwwwwwwwwwww\n"+wTmp);
  return anweihuo(x, wTmp);
}
function anweihuo(w1, w2) {
  var largeStr = w1.toString(2).length > w2.toString(2).length ? w1.toString(2) : w2.toString(2);
  var smallStr = w1.toString(2).length > w2.toString(2).length ? w2.toString(2) : w1.toString(2);

  var resStr = new Array();
  for (var i = 0; i < largeStr.length; i++) {
    if (i < largeStr.length - smallStr.length) {
      resStr.push(largeStr.substr(i, 1));
    } else {
      if (largeStr.substr(i, 1) === '1' || smallStr.substr(i - (largeStr.length - smallStr.length), 1) === '1') {
        resStr.push("1");
      } else {
        resStr.push("0");
      }
    }




  }

  var intRes = 0;
  for (var i = 0; i < resStr.length; i++) {

    intRes += parseInt(resStr[i]) * Math.pow(2, resStr.length - i - 1);

  }


  return zuoyi(intRes, 0);
}
function yuanwei(w1, w2) {
  var largeStr = w1.toString(2).length > w2.toString(2).length ? w1.toString(2) : w2.toString(2);
  var smallStr = w1.toString(2).length > w2.toString(2).length ? w2.toString(2) : w1.toString(2);
  // console.log("larStr"+largeStr+"small"+smallStr);
  var resStr = new Array();
  for (var i = 0; i < largeStr.length; i++) {
    if (i < largeStr.length - smallStr.length) {
    } else {
      if (largeStr.substr(i, 1) === '1' && smallStr.substr(i - (largeStr.length - smallStr.length), 1) === '1') {
        resStr.push("1");
      } else {
        resStr.push("0");
      }
    }

  }
  // console.log("resStr" + resStr );
  var intRes = 0;
  for (var i = 0; i < resStr.length; i++) {

    intRes += parseInt(resStr[i]) * Math.pow(2, resStr.length - i - 1);

  }


  return zuoyi(intRes, 0);
}
function feifan(x) {
  var charStr = x.toString(2);

  var resStr = new Array();
  if (charStr.length < 53) {
    for (var i = 0; i < 53 - charStr.length; i++) {
      resStr.push("1");
    }
  }

  for (var i = 0; i < charStr.length; i++) {
    if (charStr.substr(i, 1) === "1") {
      resStr.push("0");
    } else {
      resStr.push("1");
    }
  }
  var intRes = 0;
  for (var i = 0; i < resStr.length; i++) {

    intRes += parseInt(resStr[i]) * Math.pow(2, resStr.length - i - 1);

  }
  return intRes;
}
function ch(x, y, z) {
  var wTmp;
  wTmp = anweihuo(yuanwei(x, y), yuanwei(feifan(x), z));
  return zuoyi(wTmp, 0);
}
function parity(x, y, z) {
  // body...
  var wTmp;
  wTmp = yihuoyunsuan(yihuoyunsuan(x, y), z);
  return zuoyi(wTmp, 0);
}
function maj(x, y, z) {
  // body...
  var wTmp;
  wTmp = anweihuo(anweihuo(yuanwei(x, y), yuanwei(x, z)), yuanwei(y, z));
  return zuoyi(wTmp, 0);
}

function sha_ByteToWord(ptr, n) {
  var i, j;
  var pTmp;

  pTmp = ptr;
  for (i = 0; i < n; i++) {
    m[i] = 0x00000000;
    for (j = 0; j < 4; j++) {
      m[i] = zuoyi(m[i], 8);
      m[i] += pTmp[4 * i + j];
    }
  }

  // console.log("pTmp:"+pTmp);
  return;
}

function sha_ClearW() {
  // body...
  var i;
  for (i = 0; i < 80; i++) {
    w[i] = 0x00000000;

  }
  return;
}
function CopyM2W() {
  // body...
  var i;
  for (i = 0; i < 16; i++) {
    w[i] = m[i];
  }
  return;
}
function yihuoyunsuan(w1, w2) {
  var largeStr = w1.toString(2).length > w2.toString(2).length ? w1.toString(2) : w2.toString(2);
  var smallStr = w1.toString(2).length > w2.toString(2).length ? w2.toString(2) : w1.toString(2);
  var resStr = new Array();
  for (var i = 0; i < largeStr.length; i++) {
    if (i < largeStr.length - smallStr.length) {
      resStr.push(largeStr.substr(i, 1));
    } else {
      if (largeStr.substr(i, 1) !== smallStr.substr(i - (largeStr.length - smallStr.length), 1) && (largeStr.substr(i, 1) === '1' || smallStr.substr(i - (largeStr.length - smallStr.length), 1) === '1')) {
        resStr.push("1");
      } else {
        resStr.push("0");
      }
    }
  }
  // console.log(":resStr\n" + resStr);

  var intRes = 0;
  for (var i = 0; i < resStr.length; i++) {

    intRes += parseInt(resStr[i]) * Math.pow(2, resStr.length - i - 1);

  }

  //  console.log(":ssssss\n" + intRes);
  return zuoyi(intRes, 0);
}
function zuoyi(char, n) {
  var charStr = (char * Math.pow(2, n)).toString(2);
  if (charStr.length > 32) {
    charStr = charStr.substr(charStr.length - 32, 32);
  }
  var intRes = 0;
  for (var i = 0; i < charStr.length; i++) {

    intRes += parseInt(charStr[i]) * Math.pow(2, charStr.length - i - 1);

  }

  return intRes;

}
function ProChunk() {
  // body...
  var t;
  var wTmp;

  sha_ClearW();
  CopyM2W();
  // console.log("ssssssssss" + ":w[t]\n" + w);
  for (t = 16; t < 80; t++) {

    wTmp = yihuoyunsuan(yihuoyunsuan(yihuoyunsuan(w[t - 3], w[t - 8]), w[t - 14]), w[t - 16]);
    // console.log(t + ":w[t]\n" + wTmp);

    // console.log(ss+"wTmp" + wTmp);
    w[t] = zuoyi(S_LEFT(wTmp, 1), 0);
    // console.log(t + ":w[t]\n" + w[t]);
  }

  A = 0x67452301;
  B = 0xefcdab89;
  C = 0x98badcfe;
  D = 0x10325476;
  E = 0xc3d2e1f0;


  for (t = 0; t < 80; t++) {
    wTemp = S_LEFT(A, 5);
    // console.log("wtemp:" + wTemp);
    wTemp = zuoyi(wTemp + E, 0);
    // console.log(t + "wtemp:" + wTemp);


    wTemp = zuoyi(wTemp + w[t], 0);
    // console.log(t + "wtempwTempwTemp:" + wTemp);
    if (t <= 19)	//if(0<=t&&t<=19)
    {
      wTemp = zuoyi(zuoyi(wTemp + ch(B, C, D), 0) + KT1, 0);
    }
    if (20 <= t && t <= 39) {
      wTemp = zuoyi(zuoyi(wTemp + parity(B, C, D), 0) + KT2, 0);
    }
    if (40 <= t && t <= 59) {
      wTemp = zuoyi(zuoyi(wTemp + maj(B, C, D), 0) + KT3, 0);
    }
    if (60 <= t && t <= 79) {
      wTemp = zuoyi(zuoyi(wTemp + parity(B, C, D), 0) + KT4, 0);
    }
    // console.log(t + "wsssssssssstemdadwadawdp:\n" + wTemp);

    E = zuoyi(D, 0);
    D = zuoyi(C, 0);
    C = zuoyi(S_LEFT(B, 30), 0);
    B = zuoyi(A, 0);
    A = zuoyi(wTemp, 0);
  }
  // console.log("A:" + A);
  // console.log("B:" + B);
  // console.log("C:" + C);
  // console.log("D:" + D);
  return;
}
function HashSha1(ptr) {
  var lVar;

  sha_ByteToWord(ptr, 16);
  ProChunk();					// ˝æ›øÈ¥¶¿Ì

  lVar = Math.floor(A % 256);
  B /= 256;
  lVar *= 256;
  lVar += Math.floor(B % 256);
  C /= 256 * 256;
  lVar *= 256;
  lVar += Math.floor(C % 256);
  D /= 256 * 256 * 256;
  lVar *= 256;
  lVar += Math.floor(D % 256);

  // console.log("IVar:" + lVar);
  return lVar;
}





module.exports = {
  scanPage: scanPage

}