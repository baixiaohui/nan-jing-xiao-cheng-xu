
var app = getApp();
var uTil = require('../../utils/util.js');
var apiM = require('../../utils/APIManager.js');
var time;
var backPage;
var phone, code;
var uTil = require("../../utils/util.js");
Page({
  data: {
    userInfo: {},
    phone: null,
    code: null,
    message: "获取验证码",
    plain: true,
    disabled: false,
    selected: false,
    textcolor: "color:#2476ff",
    loginCode:null
  },
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh()
  },
  onLoad: function (options) {
    console.log(options.telephone);
    var that = this;
    backPage = options.backPage;
    //调用应用实例的方法获取全局数据
    if (options.telephone!=null){
      that.setData({
        phone: options.telephone
      });
    }
  },
  onReady: function () { },
  onShow: function () {
    console.log("重置获取验证码按钮");
    this.setGetVcodeBtnStatus(true, 0);

  },
  onHide: function () {
    clearInterval(time);
  },
  onUnload: function () { },
  getPhone: function (e) {
    this.setData({
      phone: e.detail.value
    });
  },
  checkcode: function (e) {
    this.setData({
      code: e.detail.value
    });
  },
  setLoginCode:function(e){
    this.setData({
      loginCode: e.detail.value
    });
  },
  //获取验证码网络请求
  getWebvcode: function (tel) {
    var that = this;
    apiM.isRegisterAct(tel, function (res) {
      if (res.retcode == "0") {
        if (res.isRegister == "0") {  //未注册
          that.setGetVcodeBtnStatus(false, 60);
          var count = 60;  //倒计时 
          time = setInterval(function () {
            if (count == 0) {
              clearInterval(time);
              that.setGetVcodeBtnStatus(true, 0);
            } else if (count > 0) {
              count = count - 1;
              that.setGetVcodeBtnStatus(false, count);
            }

          }, 1000);
          apiM.getVcode(
            tel, function (res) {
              console.log("调用getVcode返回结果");
              if (res.retcode != 0) {
                uTil.showTips(res.retmsg, "success", "../../img/error@3x.png", 1.0, true, function () {
                  clearInterval(time);
                  that.setGetVcodeBtnStatus(true, 0);
                  console.log("获取验证码失败弹出窗成功");
                },
                  null);
              } else {
                uTil.showTips("获取验证码成功", "success", null, 1, null);
              }
            }, function () {
              console.log("!!!!!!!!!");
              uTil.showTips("请检查您的网络", "fail", "../../img/error@3x.png", 1, null);
              clearInterval(time);
              that.setGetVcodeBtnStatus(true, 0);

            });
        } else if (res.isRegister == "1") { //已注册
          uTil.showTips("用户已注册请直接登录", "fail", "../../img/error@3x.png", 1, null);
        }
      } else {
        uTil.showTips("请求数据失败", "fail", "../../img/error@3x.png", 1, null);
      }
    })
    
  },
  //获取验证码之前的手机号判断和状态改变
  getcheckcode: function (e) {
    var that = this;
    if (that.data.selected == true) {  //防止重复点击
      console.log("重复点击");
      return;
    } else {
      that.setData({
        selected: false
      });
    }
    var tel = this.data.phone;
    if (this.isMobilePhone(tel) == true) {
     
      that.getWebvcode(tel);

    } else {
      uTil.showTips("请填写正确手机号", "fail", "../../img/error@3x.png", 1, null);
      return;
    }
  },
  //设置获取验证码按钮可点击
  setGetVcodeBtnStatus: function (sure, count) {
    var that = this;
    if (sure) {
      console.log("_________________>>>>>>>>");
      that.setData({
        message: "获取验证码",
        disabled: false,
        selected: false,
        textcolor: "color:#2476ff"
      });
    } else {
      that.setData({
        message: count + "s后重新获取",
        disabled: true,
        selected: true,
        textcolor: "color:#ddd"
      });
    }
  },
  //立即注册
  registerBtn: function (phone, code, loginCode) {
    phone = this.data.phone;
    code = this.data.code;
    loginCode = this.data.loginCode;
    var that = this;
    that.checkTelCode(phone, code, loginCode, function () {
      that.getLoginApi(phone, code, loginCode);
    });
    // 调取登录接口
  },
  checkTelCode: function (phone, code, loginCode, callback) {
    if (!phone || !code || !loginCode) {
      uTil.showTips("验证码或手机号或登录密码缺失", "fail", "../../img/error@3x.png", 1, null);
      return;
    }
    if (!this.isMobilePhone(phone)) {
      uTil.showTips("请填写正确的手机号", "fail", "../../img/error@3x.png", 1, null);
      return;
    } else {
      callback();
      // wx.setStorageSync('telephone', phone);
      // this.loginSuccessMsg();  //暂时 登录成功提示框
    }
  },

  //注册api
  getLoginApi: function (phone, code,loginCode) {
    var that = this;
    wx.showLoading({
      title: '登录中...',
    });
    apiM.register(
      phone, code, loginCode, function (res) {
        if (res.retcode == 0) {
            wx.navigateTo({
              url: '../index/index',
            })
        } else if (res.retcode == 3010) {
            uTil.showTips("手机号验证码不匹配", "fail", "../../img/error@3x.png", 1, null);
        }
      });
  },
  
  // 判断是否为手机号
  isMobilePhone: function (phone) {
    var re = /^1(3|4|5|7|8)\d{9}$/;
    if (re.test(phone)) {
      return true;
    } else {
      return false;
    }
  },
})
