// pages/search/search.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabarr:[
      {id: "0",name: "基本"},
      { id: "1", name: "租车" },
      { id: "2", name: "保证金" }
    ],
    currentid:0,
    basicArr:[
      { id:"0",num:"001",one: "如何开通",two:"如何注册、开通租车功能"},
      { id: "0", num: "002",one: "如何租车", two: "如何租车" },
      { id: "0", num: "003",one: "如何还车", two: "如何还车" },
      { id: "0", num: "004",one: "如何支付", two: "超时费怎么支付" },
      // { id: "0", num: "005",one: "如何导航", two: "如何导航" }
    ],
    normalArr: [
      { id: "1", num: "101",one: "租车数量", two: "一个账号能租几辆车" },
      { id: "1", num: "102",one: "收费标准", two: "租车时间、收费标准" },
      { id: "1", num: "103",one: "租车异常", two: "租不了车怎么办" },
      // { id: "1", num: "104",one: "还车计时", two: "还车成功，仍然在倒计时" },
      // { id: "1", num: "105",one: "扫码失败", two: "扫码出现编号什么情况" }
    ],
    depositArr: [
      { id: "2", num: "201",one: "退还押金", two: "如何退还保证金" },
      // { id: "2", num: "202",one: "退还时间", two: "申请退还保证金后，多久退回" },
      // { id: "2", num: "203",one: "开通问题", two: "已缴保证金，但未开通怎么办" }
     
    ],

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this;
   
  },
  bindchange:function(e){
    this.setData({
      pagenow: e.currentTarget.id
    })
  },
  swmove:function(e){
     this.setData({ 
       currentid:e.detail.current
     })
  },
  clickit:function(e){
    var _id = e.currentTarget.dataset.id;
    var _num = e.currentTarget.dataset.num;
     console.log(_id);
     console.log(_num);
     wx.navigateTo({
       url: '../scroll/scroll?id='+ _id + "&num=" + _num,
       success: function (res) {
         // success
       },
       fail: function () {
         // fail
       },
       complete: function () {
         // complete
       }
     })

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  }
})