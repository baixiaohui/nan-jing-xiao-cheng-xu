//myCenter.js
var app = getApp();
var uTil = require('../../utils/util.js');
var pingpp = require('../../utils/pingpp.js');
var apiM = require('../../utils/APIManager.js');
Page({
  data: {
    userInfo: {},
    src: '../../img/icon_line0@2x.png',
    tripArray: [],
    toPay: false,
    src: "",
    isShowImg: false
  },
  onLoad: function () {
    wx.stopPullDownRefresh();
    var that = this;
    that.getTripRecord();
  },

  //获取支付凭证
  getCharge: function (orderid) {
    var that = this;
    apiM.getCharge(
      orderid, 'wx_lite', 'wx_lite'
      , function (res) {
        console.log(res.retcode + "getCharge");
        console.log(res.chargedata);
        pingpp.createPayment(res.chargedata, function (result, err) {
          console.log("pingpp++++++++");
          console.log(pingpp);
          if (result == "success") {
            that.showMessage("开通租车业务成功,快去租车吧！");
            //  refreshStatus(orderid);
          } else if (result == "fail") {
            // refreshStatus(orderid);
            that.showMessage("支付失败");
            // refreshStatus(orderid);
          } else if (result == "cancel") {
            that.showMessage("用户取消支付");
            //  refreshStatus(orderid);
          } else {
            // refreshStatus(orderid);
          }
        });
      });
  },
  getOrdersAct: function () {
    var that = this;
    apiM.getordersAction(
      "2", function (res) {
        console.log(res.retcode + "getOrdersAct");
        console.log(res);
        var curOrdeid;
        for (var i = 0; i < res.data.length; i++) {
          if (res.data[i].orderstatus == "1" || res.data[i].orderstatus == "2") {
            curOrdeid = res.data[i].orderid;
            break;
          }
        }
        that.getCharge(curOrdeid);
      });
  },

  //获取行程记录
  getTripRecord: function () {
    var that = this;
    uTil.showLoadingDD("获取行程记录...", true, function(){
      apiM.getRecord("10", function (res) {
        if (res.retcode == "0") {
          wx.hideLoading();
          var array = res.data;
          if (res.data) {
            for (var i = 0; i < array.length; i++) {
              var startTime = array[i].renttime;
              var endTime = array[i].restoretime;
              array[i].userTime = '';
              // array[i].userTime = that.compareTime(startTime, endTime);
              console.log(array[i].userTime);
            }
            for (var i = 0; i < array.length; i++) {
              switch (array[i].hirestatus) {
                case "1":
                  res.data[i].hirestatus = "已租";
                  break;
                case "2":
                  res.data[i].hirestatus = "已还";
                  break;
                case "5":
                  res.data[i].hirestatus = "异常租车";
                  break;
                case "6":
                  res.data[i].hirestatus = "异常还车";
                  break;
                case "11":
                  res.data[i].hirestatus = "临时停车";
                  break; 
                default:
                  res.data[i].hirestatus = "异常数据";
                  break;
              }
            }
            that.setData({
              tripArray: array,
              isShowImg: false
            })
            wx.setStorageSync("bikeid", res.data[0].bikeid);
          } else {
            that.setData({
              src: "../../img/img_route@2x.png",
              isShowImg: true
            });
          }
        } else {
          wx.hideLoading();
          uTil.showTips(res.retmsg, "success", null, 1, null);
        }
      });
    }, null);
    
  },

  //请求支付（点击去支付按钮绑定事件）
  topayBtn: function (e) {
    var that = this;
    that.getOrdersAct();
  },

  refreshStatus: function () {
    var that = this;
    var orderId = wx.getStorageSync('openOrderId');
    that.checkOrder(orderId); //租车都要取 openOrderId 
    that.switchUserStatus();//判断租车状态bizStatus刷新页面
  },

  //计算时间差
  compareTime: function (date1, date2) {
    var s1 = new Date(date1.replace(/-/g, '/')).getTime();
    var s2 = new Date(date2.replace(/-/g, '/')).getTime();
    var total = (s2 - s1) / 1000;
    var day = parseInt(total / (24 * 60 * 60));
    var afterDay = total - day * 24 * 60 * 60;
    var hour = parseInt(afterDay / (60 * 60));
    var afterHour = total - day * 24 * 60 * 60 - hour * 60 * 60;
    var min = parseInt(afterHour / 60);//计算整数分
    var afterMin = total - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60;
    if (hour <= "0") {
      return min + "分" + afterMin + "秒";
    } else {
      return hour + "小时" + min + "分" + afterMin + "秒";
    }

  },

  onPullDownRefresh() {
    console.log('--------下拉刷新-------')
    wx.showNavigationBarLoading() //在标题栏中显示加载
  },
  //提示框
  showMessage: function (msg) {
    wx.showLoading({
      title: msg,
    });
    setTimeout(function () {
      wx.hideLoading()
    }, 2000)
  },
  onReady: function () { },
  onShow: function () { },
  onHide: function () { },
  onUnload: function () { }
})
