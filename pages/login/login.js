//myCenter.js
var app = getApp();
var uTil = require('../../utils/util.js');
var apiM = require('../../utils/APIManager.js');
var time;
var backPage;
var phone, code;
var uTil = require("../../utils/util.js");
Page({
  data: {
    userInfo: {},
    phone: null,
    code: null,
    secret:null,
    message: "获取验证码",
    plain: true,
    disabled: false,
    selected: false,
    textcolor: "color:#2476ff"
  },
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh()
  },
  onLoad: function (options) {
    var that = this;
    backPage = options.backPage;
    //调用应用实例的方法获取全局数据
    var that = this;
  },
  onReady: function () { },
  onShow: function () {
   // console.log("重置获取验证码按钮");
    //this.setGetVcodeBtnStatus(true, 0);

  },
  onHide: function () {
    //clearInterval(time);
  },
  onUnload: function () { },
  getPhone: function (e) {
    this.setData({
      phone: e.detail.value
    });
  },
  checkcode: function (e) {
    this.setData({
      code: e.detail.value
    });
  },
  getsecret: function (e) {
    this.setData({
      secret: e.detail.value
    });
  },
  //获取验证码网络请求
  getWebvcode: function (tel) {
    var that = this;

    apiM.getVcode(
      tel, function (res) {
        console.log("调用getVcode返回结果");
        if (res.retcode != 0) {
          uTil.showTips(res.retmsg, "success", "../../img/error@3x.png", 1.0, true, function () {
            clearInterval(time);
            that.setGetVcodeBtnStatus(true, 0);
            console.log("获取验证码失败弹出窗成功");
          },
          null);
        } else {
          uTil.showTips("获取验证码成功", "success", null, 1, null);
        }
      }, function () {
        console.log("!!!!!!!!!");
        uTil.showTips("请检查您的网络", "fail", "../../img/error@3x.png", 1, null);
        clearInterval(time);
        that.setGetVcodeBtnStatus(true, 0);

      });
  },
  //获取验证码之前的手机号判断和状态改变
  getcheckcode: function (e) {
    var that = this;
    if (that.data.selected == true) {  //防止重复点击
      console.log("重复点击");
      return;
    } else {
      that.setData({
        selected: false
      });
    }
    var tel = this.data.phone;
    if (this.isMobilePhone(tel) == true) {
      that.setGetVcodeBtnStatus(false, 60);
      var count = 60;  //倒计时 
      time = setInterval(function () {
        if (count == 0) {
          clearInterval(time);
          that.setGetVcodeBtnStatus(true, 0);
        } else if (count > 0) {
          count = count - 1;
          that.setGetVcodeBtnStatus(false, count);
        }

      }, 1000);
      that.getWebvcode(tel);

    } else {
      uTil.showTips("请填写正确手机号", "fail", "../../img/error@3x.png", 1, null);
      return;
    }
  },
  //设置获取验证码按钮可点击
  setGetVcodeBtnStatus: function (sure, count) {

    var that = this;
    if (sure) {
      console.log("_________________>>>>>>>>");
      that.setData({
        message: "获取验证码",
        disabled: false,
        selected: false,
        textcolor: "color:#2476ff"
      });
    } else {
      that.setData({
        message: count + "s后重新获取",
        disabled: true,
        selected: true,
        textcolor: "color:#ddd"
      });
    }
  },
 
  // loadin: function (phone, code) {
  //   phone = this.data.phone;
  //   code = this.data.code;
  //   var that = this;
  //   that.checkTelCode(phone, code, function () {
  //     that.getLoginApi(phone, code);
  //   });
  //   // 调取登录接口
  // },
   //登录
  loadin: function (phone, secret) {
    phone = this.data.phone;
    secret = this.data.secret;
    var that = this;
    that.checkTelCode(phone, secret, function () {
      that.getLoginApi(phone, secret);
    });
    // that.checkTelCode(phone, secret, function () {
    //   that.getLoginApi(phone, secret);
    // });
    // 调取登录接口
  },
  checkTelCode: function (phone, secret, callback) {
    if (!phone || !secret) {
      uTil.showTips("手机号或密码缺失", "fail", "../../img/error@3x.png", 1, null);
      return;
    }
    if (!this.isMobilePhone(phone)) {
      uTil.showTips("请填写正确的手机号", "fail", "../../img/error@3x.png", 1, null);
      return;
    } else {
      callback();
      // wx.setStorageSync('telephone', phone);
      // this.loginSuccessMsg();  //暂时 登录成功提示框
    }
  },
  

  //登录api
  getLoginApi: function (phone, secret) {
    var that = this;
    wx.showLoading({
      title: '登录中...',
    });
    apiM.isRegisterAct(phone, function (res) {
      if (res.retcode == "0") {
        if (res.isRegister == "0") {  //未注册
          uTil.showTips("用户未注册请先注册", "fail", "../../img/error@3x.png", 1, null);
        } else if (res.isRegister == "1") { //已注册
          apiM.login(
            phone, secret, function (res) {
              if (res.retcode == 0) {
                var success = true;
                apiM.getBizInfo(function (resbizinfo) {
                  if (resbizinfo.retcode != 0) {
                    uTil.showTips("登录失败请重试", "fail", "../../img/error@3x.png", 1, null);
                  } else {
                    apiM.getCityInfo(function (rescityinfo) {
                      if (rescityinfo.retcode != 0) {
                        uTil.showTips("登录失败请重试", "fail", "../../img/error@3x.png", 1, null);
                      } else {
                        wx.navigateBack({
                          url: '../index/index'
                        });
                      }
                    });
                  }
                });
              } else if (res.retcode == 3010) {

                uTil.showTips("手机号密码不匹配", "fail", "../../img/error@3x.png", 1, null);
              } else if (res.retcode == 3005) {

                uTil.showTips("密码错误", "fail", "../../img/error@3x.png", 1, null);
              }
            });
        }
      } else {
        uTil.showTips("请求数据失败!!", "fail", "../../img/error@3x.png", 1, null);
      }
    })
   
  },

  // 判断是否为手机号
  isMobilePhone: function (phone) {
    var re = /^1(3|4|5|7|8)\d{9}$/;
    if (re.test(phone)) {
      return true;
    } else {
      return false;
    }
  },
})
