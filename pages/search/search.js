// pages/search/search.js
var QQMapWX = require('../../utils/qqmap-wx-jssdk.js');
var apiM = require('../../utils/APIManager.js');
var localinf = require('../../utils/LocalInfoManager.js');

var app = getApp();
Page({
  data: {
    keywords: null,
    isor: "none",
    results: [],
    clearor: 1,
    searchStake: 1,
  },
  onLoad: function (options) {
    var that = this;
    console.log("跳转到搜索页面" + options.stake);
    that.setData({
      searchStake: options.stake
    });
    localinf.readHistory(that.data.searchStake, function (historyarray) {
      that.setData({
        results: historyarray
      });
      console.log("存在历史记录");
    });
    var val = that.data.results;
    if (val.length == 0) {
      that.setData({
        clearor: 1
      })
    } else {
      that.setData({
        clearor: 2
      })
    }
  },
  focustap: function (e) {

    this.setData({
      isor: "block"
    })

  },
  blurtap: function (e) {
    this.setData({
      isor: "none"
    });
  },
  //点击软键盘搜索按钮
  getInput: function (e) {
    var that = this;
    that.setData({
      keywords: e.detail.value
    });
    console.log(that.data.keywords);
    var key = that.data.keywords;
    if (!key) { return; }
    if (that.data.searchStake == 1) {
      console.log("搜索有桩站点");
      apiM.searchStions(key, function (res) {
        that.setData({
          results: res.data
        });
      })
    } else {
      console.log("搜索无桩桩站点");
      apiM.getNearStationWithoutStake(0, 0, key, function (res) {
        that.setData({
          results: res.data
        });
      });
    }
  },
  clearvalue: function () {
    this.setData({
      keywords: null
    });
  },
  cancelBtn: function () {
    wx.navigateBack({
      delta: 1
    })
  },
  clearHistory: function () {
    console.log("清除历史记录");
    localinf.cleanHistory(that.data.searchStake);
    this.setData({
      results: [],
      clearor: 1
    });
  },
  searchSite: function (e) {
    var that = this;
    var _bikeid = e.currentTarget.id;
    var _lng, _lat;
    var array = that.data.results;
    for (var i = 0; i < array.length; i++) {
      if (that.data.searchStake == 1) {
        if (_bikeid == array[i].number) {
          wx.setStorageSync('searchLng', array[i].lon);
          wx.setStorageSync('searchLat', array[i].lat);
          app.data.fromid = 1;
          localinf.searchHistorysave(that.data.searchStake, array[i]);
          wx.navigateBack({
            url: '../../index/index'
          });
          return;
        }
      } else {
        if (_bikeid == array[i].deviceId) {
          var lonlatstring = array[i].coordinate.split(",");
          wx.setStorageSync('searchLng', Number(lonlatstring[0]));
          wx.setStorageSync('searchLat', Number(lonlatstring[1]));
          app.data.fromid = 1;
          localinf.searchHistorysave(that.data.searchStake, array[i]);
          wx.navigateBack({
            url: '../../index/index'
          });
          return;
        }
      }

    }
  },
  onReady: function () { },
  onShow: function () { },
  onHide: function () { },
  onUnload: function () { },
  onPullDownRefresh: function () { },
  onReachBottom: function () { },
  onShareAppMessage: function () { },
  onReachBottom: function () { },
  onPullDownRefresh: function () { }
})