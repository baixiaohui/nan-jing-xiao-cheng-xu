//myCenter.js
var app = getApp();
var uTil = require('../../utils/util.js');
var pingpp = require('../../utils/pingpp.js');
var apiM = require('../../utils/APIManager.js');
Page({
  data: {
    userInfo: {},
    src:'../../img/icon_line0@2x.png',
    orderArray:[],
    toPay:false,
    src:"",
    isReact:false
  },
  onLoad: function () { 
    var that = this;
    uTil.showLoadingDD("获取订单列表...", true, null, null);
    that.getordersEvent();
    wx.stopPullDownRefresh();
  },
 
  //获取getOrders渲染页面
  getordersEvent:function(){
    var that=this;
    
    apiM.getordersAction(
      "2", function (res) {
        wx.hideLoading();
        if(res.retcode=="0"){
          
          if(res.data.length>0){   
            console.log("获取订单列表数据大于0 ");
            for (var i = 0; i < res.data.length; i++) {
              switch (res.data[i].orderstatus) {
                case "1":
                  res.data[i].orderstatus = "订单未支付";//新建
                  break;
                case "2":
                  res.data[i].orderstatus = "订单未支付";//已提交
                  break;
                case "3":
                  res.data[i].orderstatus = "订单已完成";
                  break;
                case "4":
                  res.data[i].orderstatus = "订单已取消";
                  break;
              }
            }
          that.setData({
            orderArray: res.data,
            isReact:false
          })
          } else {
            that.setData({
              src:"../../img/img_route@2x.png",
              isReact: true
            });  
          }
        }else{
    
          uTil.showTips("获取订单列表失败...", "fail", "../../img/error@3x.png", 1, null);
        }
       
      });
  },
  //点击去支付按钮绑定事件
  toPayBtn:function(e){
    var id=e.currentTarget.id;
    var that = this;
    that.getCharge(id);
  },

  onPullDownRefresh() {
    console.log('--------下拉刷新-------')
    wx.showNavigationBarLoading() //在标题栏中显示加载
  },

  

  //请求支付getCharge,调用pingpp
  getCharge: function (orderid) {
    var that = this;
    apiM.getCharge(
      orderid, 'wx_lite'
      , function (res) {
        console.log(res.retcode + "getCharge");
        console.log(res.chargedata);
        // var chargedata = JSON.parse(res.chargeData);  问题：JSON,parse不识别
        if (res.retcode == "0") {
          pingpp.createPayment(res.chargedata, function (result, err) {
            console.log(result+"pingpp");
            console.log(pingpp);
            if (result == "success") {
              uTil.showTips("支付成功！", "success", null, 1, null);
              that.setData({
                orderArray: [] 
              });
          
                that.getordersEvent();
            
            } else if (result == "fail") {
            
              that.refreshStatus(orderid);
            } else if (result == "cancel") {
              
            }
          });
        } else {
          that.showMessage("获取支付数据失败");
        }
      });
  },
   refreshStatus: function (orderid) {
    var that = this;
    that.checkOrderEvent(orderid);
    
  },
 
  //检查订单状态
  checkOrderEvent: function (orderId) {
    var that = this;
    apiM.checkOrderAction(
      orderId, function (res) {
        if (res.retcode == "0") { 
              apiM.getBizInfo(function (bizinfo) {
                wx.setStorageSync("bizinfo", bizinfo);
                that.getordersEvent(); 
              })    
        } else {

         }
      });
  },
 
  //提示框
  showMessage: function (msg) {
    wx.showLoading({
      title: msg,
    });
    setTimeout(function () {
      wx.hideLoading()
    }, 2000)
  },
  onReady: function () {},
  onShow: function () {
    
  },
  onHide: function () { },
  onUnload: function () {}
})
