// 引用百度地图微信小程序JSAPI模块 
var bmap = require('../../utils/bmap-wx.js');
var wxMarkerData = [];//BAIDU获取定位后的数据
var app = getApp();
var backmyCenter = '../login/login?backPage=../myCenter/myCenter';
var uTil = require('../../utils/util.js');
var scan_code = require('../../utils/scan_qrcode.js');
var QQMapWX = require('../../utils/qqmap-wx-jssdk.js');
var currentmarkerInfo = {};
var apiM = require('../../utils/APIManager.js');
var demo = new QQMapWX({
  key: 'T6DBZ-GJBWP-OECDP-VQV46-CXBFO-HZFLS'
});
var model = ('../../utils/Model.js');

var bizStatus;
Page({
  data: {
    markers: [],
    latitude: '',
    longitude: '',
    userCanTouch: false,
    rgcData: {},
    mapHeight: null,
    mapWidth: null,
    bmap: null,
    searchLng: null,
    searchLat: null,
    controls: [],
    callout: [],

    isHaveStake: 1, //有桩无桩区分 1为有桩 3 为无桩 2 GPS桩
    windowHeight: 0,
    windowWidth: 0,
    isTmpStop: false,
    isRentbike: false,  //是否租车  默认未租车
    isHireStake: 1,
    centerLon: null, //地图中心点经度
    centerLat: null, //地图中心点纬度
    isNeedPay: false,

    markerdata: { //设置当前marker
      distance: 0,
      rentcount: 0,
      restorecount: 0,
      lon: null,
      lat: null,
      bikebnumber: 0,
      name: "",
      deviceName: ''
    },
    lastpointLon: null,
    lastpointLat: null,
    uNit: "",
    allmarkerDatainfo: [],//所有的maker的所有数据
    isClickMarker: false, //是否点击marker 默认有桩为true
    noStakeDistance: "822", //无桩站点距离
    noStakeDistanceNum: null,
    noStakePrice: "0.5",
    serviceId: "",
  },
  onReady: function (e) {
    // 使用 wx.createMapContext 获取 map 上下文
    this.mapCtx = wx.createMapContext('map');
    this.baiduArr = [];
    this.isTapSelected = false
  },
  onShow: function () {
    var that = this;
    that.updatesysteminfo();
    app.updateBizinfo(function () {
      that.initUserStatus();
    });
    if (app.data.fromid == 1) {
      console.log("显示搜索站点" + wx.getStorageSync("searchLng") + "1111");
      app.data.fromid = 0;
      that.setData({
        longitude: wx.getStorageSync("searchLng"),
        latitude: wx.getStorageSync("searchLat"),
        centerLon: wx.getStorageSync("searchLng"),
        centerLat: wx.getStorageSync("searchLat"),
        lastpointLon: wx.getStorageSync("searchLng"),
        lastpointLat: wx.getStorageSync("searchLat")
      });
      that.getNearStgetneaation();
    }
  },
  //初始化
  onLoad: function (options) {
    var that = this;
    if (wx.getStorageSync("isHaveStake")) {
      console.log(wx.getStorageSync("isHaveStake") + "---------->>>>>>>>>>>");
      that.setData({
        isHaveStake: wx.getStorageSync("isHaveStake")
      })
    }
    that.initMap();
    that.getUserLocation();
    that.setTheControls(that.data.isHaveStake);

  },
  //初始化地图和地图上的控件
  initMap: function () {
    var that = this;
    this.data.bmap = new bmap.BMapWX({
      ak: 'LTjqCGGMT237OEqLnQiMhtmecZDi5eGV',
    });
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          windowHeight: res.windowHeight,
          windowWidth: res.windowWidth,
          mapWidth: res.windowWidth,
          mapHeight: res.windowHeight - 50,
        });
      }
    });
  },
  ///设置用户状态
  initUserStatus: function () {
    var that = this;
    var bizinfo = wx.getStorageSync("bizinfo");
    if (bizinfo.bizStatus == 1004) {
      var extraStatus = JSON.parse(bizinfo.bizExtra).hireStatus;
      if (extraStatus == "31") {
        that.setData({
          isHireStake: 3
        });
      } else {
        that.setData({
          isHireStake: 1
        });
      }
      if (wx.getStorageSync("isTemporaryBike") == "true") {
        that.setData({
          isTmpStop: true
        });
      }else {
        that.setData({
          isTmpStop: false
        });
      }
      that.setData({
        isRentbike: true,
        isNeedPay: false
      });
    } else if (bizinfo.bizStatus == 1005) {
      console.log("未支付订单>>>>>.+");
      console.log(bizinfo);
      that.setData({
        isRentbike: false,
        isNeedPay: true
      });
    } else {
      that.setData({
        isRentbike: false,
        isNeedPay: false
      });
    }
  },
  //定位到当前用户
  getUserLocation: function () {
    var that = this;
    //定义定位失败方法
    var fail = function (data) {
      console.log(data)
    };
    //定义定位成功方法
    var success = function (data) {
      console.log(data);
      wxMarkerData = data.wxMarkerData;
      console.log("sessionid_==================" + wxMarkerData[0].longitude);
      that.setData({
        latitude: wxMarkerData[0].latitude,
        longitude: wxMarkerData[0].longitude,
        centerLon: wxMarkerData[0].longitude,
        centerLat: wxMarkerData[0].latitude,
        lastpointLon: wxMarkerData[0].longitude,
        lastpointLat: wxMarkerData[0].latitude
      });
      wx.setStorageSync('lng', wxMarkerData[0].longitude);
      wx.setStorageSync('lat', wxMarkerData[0].latitude);
      that.getNearStgetneaation();
    }
    // 发起定位 
    that.data.bmap.regeocoding({
      fail: fail,
      success: success
    });
    that.mapCtx.moveToLocation();
  },

  //根据站点信息编辑marker和callout
  managerMarkerAndCallout: function () {
  },
  //获取周边网点数据
  getNearStgetneaation: function (stakeStatus) {   
    var that = this;
    if (that.data.isHaveStake == 1) {
      apiM.getNearSation(that.data.centerLon, that.data.centerLat, String(that.data.isHaveStake), function (res) {
        if (res.retcode == "103") {
          console.log("请求附近站点错误：：：" + res.retcode);
          that.isTapSelected = false
        } else {
          that.setTheMapMarkers(res.data);
        }
      });
    } else {
      apiM.getNearStationWithoutStake(that.data.centerLon, that.data.centerLat, "", function (res) {
        if (res.retcode == "103") {
          console.log("请求附近站点错误：：：" + res.retcode);
          that.isTapSelected = false
        } else {
          that.setTheMapMarkers(res.data);
        }
      });
    }

  },
  //闪电找车
  flashFindBike: function () {
    var that = this;
    var markersData_ = this.data.allmarkerDatainfo;
    if (markersData_.length <= 0) {
      return;
    }
    var _centerlon = that.data.centerLon;
    var _centerlat = that.data.centerLat;
    var flashDistance = 0;
    var index = 0;
    console.log(markersData_[0]);
    if (that.data.isHaveStake == 1) {
      that.getPointDistance(_centerlat, _centerlon, markersData_[0].lat, markersData_[0].lon, function (distance) {
        flashDistance = distance;
        for (var i = 1; i < markersData_.length; i++) {
          that.getPointDistance(_centerlon, _centerlat, markersData_[i].lon, markersData_[i].lat, function (oneDistance) {
            index = oneDistance <= flashDistance ? i : index;
            flashDistance = oneDistance <= flashDistance ? oneDistance : flashDistance;
          });
        }
      });
    } else {
      var lonlatstring = markersData_[0].coordinate.split(",");
      that.getPointDistance(_centerlat, _centerlon, Number(lonlatstring[0]), Number(lonlatstring[1]), function (distance) {

        flashDistance = distance;
        for (var i = 1; i < markersData_.length; i++) {
          var lonlatstringtmp = markersData_[i].coordinate.split(",");
          that.getPointDistance(_centerlon, _centerlat, Number(lonlatstringtmp[0]), Number(lonlatstringtmp[1]), function (oneDistance) {

            index = oneDistance <= flashDistance ? i : index;
            flashDistance = oneDistance <= flashDistance ? oneDistance : flashDistance;
          });
        }
      });

    }
    var dataArr = that.data.allmarkerDatainfo[index];

    that.setData({
      isClickMarker: true
    });
    if (that.data.isHaveStake == 1) {
      that.comeBigMark(Number(dataArr.number));
    } else {
      that.comeBigMark(Number(dataArr.deviceId));
    }

  },
  //设置有桩无桩
  setTheControls: function (bool) {
    var that = this;
    var tmppath = "";
    if (bool == 1) {
      tmppath = "../../img/icon_chose1@2x.png";
    } else {
      tmppath = "../../img/icon_chose2@2x.png";
    }
    that.setData({
      controls: [
        {
          id: 1,
          iconPath: "../../img/icon_location0@2x.png",
          position: {
            left: 15,
            top: that.data.windowHeight - 110 - 44,
            width: 40,
            height: 40
          },
          clickable: true
        },
        {
          id: 2,
          iconPath: "../../img/scan@3x.png",
          position: {
            left: that.data.windowWidth / 2 - 60,
            top: that.data.windowHeight - 60 - 44,
            width: 120,
            height: 40
          },
          clickable: true
        },
        {
          id: 3,
          iconPath: tmppath,
          position: {
            left: 15,
            top: that.data.windowHeight - 60 - 44,
            width: 40,
            height: 40
          },
          clickable: true
        },
        {
          id: 4,
          iconPath: "../../img/icon_people@2x.png",
          position: {
            left: that.data.windowWidth - 15 - 40,
            top: that.data.windowHeight - 60 - 44,
            width: 40,
            height: 40
          },
          clickable: true
        },
        {
          id: 6,
          iconPath: '../../img/icon_flash0@2x.png',
          position: {
            left: that.data.windowWidth / 2 - 20,
            top: that.data.windowHeight / 2 - 49 - 22,
            width: 40,
            height: 49
          },
          clickable: true
        }
      ]
    });
  },
  ///清空markers
  cleanMarkers: function () {
    this.setData({
      markers: [],
      markerdata: { //设置当前marker
        distance: 0,
        rentcount: 0,
        restorecount: 0,
        lon: null,
        lat: null,
        bikebnumber: 0,
        name: ""
      },
      allmarkerDatainfo: [],
      isClickMarker: false
    })
  },
  //切换有桩无桩   isClickMarker为false  暂时true仅为测试用
  tapStake: function () {
    var that = this;
    if (that.isTapSelected == true) {
  
    }else {
      that.cleanMarkers();
      that.isTapSelected = true;
      that.setData({
        isHaveStake: that.data.isHaveStake == 3 ? 1 : 3,
      });
      wx.setStorageSync('isHaveStake', that.data.isHaveStake);
      that.getNearStgetneaation();
      that.setTheControls(that.data.isHaveStake);
    } 
  },
  //设置markes
  setTheMapMarkers: function (locArray) {
    var that = this;
    that.setData({
      markers: [],
      allmarkerDatainfo: []
    });
    if (locArray.length > 0) {
      var pointArr = [];
      var arrlength = locArray.length// > 15 ? 15 : locArray.length
      if (that.data.isHaveStake == 1) {
        arrlength = locArray.length > 15 ? 15 : locArray.length
      }
      for (var i = 0; i < arrlength; i++) {
        var eachstr;
        if (that.data.isHaveStake == 1) {
          eachstr = locArray[i].lat + "," + locArray[i].lon

        } else {
          var lonlatstring = locArray[i].coordinate.split(",");
          eachstr = lonlatstring[1] + "," + lonlatstring[0]
        }
        pointArr.push(eachstr)
      }
      var markersArr = [];
      that.baiduToTxPoint(pointArr, function (location) {
        for (var i = 0; i < location.length; i++) {
          var makersObject = null;
          if (that.data.isHaveStake == 1) {
            var tmp = locArray[i];
            var percent, markericon, restorecount, rentcount, allCount;
            restorecount = parseInt(tmp.restorecount),
              rentcount = parseInt(tmp.rentcount),
              allCount = restorecount + rentcount;
            var totalcount = parseInt(tmp.totalcount);
            totalcount = (totalcount == 0 || totalcount < allCount) ? allCount : totalcount;
            tmp.totalcount = totalcount;
            if (totalcount > 0) {
              percent = parseInt(Math.floor(rentcount * 10 / totalcount) * 10);
            }
            if (percent == 0 && rentcount > 0) {//只要有一条可租就不是零储
              percent = 10;
            }
            markericon = "../../img/0@3x.png";
            if (tmp.status == "9") {//7=正常，8=异常，9=断线
              markericon = "../../img/0@3x.png";
            } else if (percent < 0 || percent > 100) {
              markericon = "../../img/icon_location1@3x.png";
            }
            else if (percent == 0) {
              markericon = "../../img/0@3x.png";
            }
            else if (percent > 0 && percent <= 10) {
              markericon = "../../img/10@3x.png";
            }
            else if (percent > 10 && percent <= 20) {
              markericon = "../../img/20@3x.png";
            } else if (percent > 20 && percent <= 30) {
              markericon = "../../img/30@3x.png";
            } else if (percent > 30 && percent <= 50) {
              markericon = "../../img/50@3x.png";
            } else if (percent > 50 && percent <= 70) {
              markericon = "../../img/70@3x.png";
            } else if (percent > 70 && percent <= 80) {
              markericon = "../../img/80@3x.png";
            } else if (percent > 80 && percent <= 90) {
              markericon = "../../img/90@3x.png";
            } else if (percent > 90) {
              markericon = "../../img/100@3x.png";
            }
            var maskId = Number(locArray[i].number)
            makersObject = {
              id: maskId,
              iconPath: "../../img/icon_bike3@3x.png"
              , longitude: location[i].lng
              , latitude: location[i].lat
              , width: 45.9
              , height: 51.3
            };
          } else {
            var maskId = Number(locArray[i].deviceId)         
            makersObject = {
              id: maskId,
              iconPath: "../../img/icon_chose2@2x.png"//"../../img/post@3x.png"
              , longitude: location[i].lng
              , latitude: location[i].lat
              , width: 36
              , height: 37
            };
          }
          markersArr.push(makersObject);
        }
        that.setData({
          markers: markersArr,
          allmarkerDatainfo: locArray,
        });
        that.isTapSelected = false;
        console.log("that.data.allmarkerDatainfo:::", that.data.markers);
      })
    }else {
      that.isTapSelected = false;
    }
  },
  baiduToTxPoint: function (arr, callback) {
   var that = this;
   var bigArr = [];
   var smallArr = [];
   var arrCount = 1;
   var splitCount = 15;
   var locationsArrStr;
   if (arr.length > 14) {
     if (arr.length % splitCount == 0) {
       arrCount = parseInt(arr.length / splitCount);
     } else {
       arrCount = parseInt(arr.length / splitCount) + 1;
     }
     
   }
   for (var j = 0; j < arrCount; j++) {
     locationsArrStr = arr[splitCount * j];
     var smallLen = splitCount * (j + 1);
     if (splitCount*(j+1)>arr.length){
       smallLen = arr.length;
     }
     for (var i = splitCount * j + 1; i < smallLen; i++) {
       locationsArrStr = locationsArrStr + ";" + arr[i]
     }
     console.log(locationsArrStr);
     smallArr.push(locationsArrStr);
   }
   that.baiduArr = [];
   that.baiduExchange(0, smallArr,function(array){
     var newArr=[];
     for (var t = 0; t < array.length; t++) {
       newArr = newArr.concat(array[t]);
     }
     console.log('newArr====' + newArr);
     callback(newArr);
   })
  },
  baiduExchange: function (p, smallArr, callback){
    var that = this;
     demo.changePoint({
       locations: smallArr[p],
       type: 3,
       success: function (res) {
         console.log('changePoint==', res)
         that.baiduArr.push(res.locations);
        p++;
        if (p < smallArr.length) {
        that.baiduExchange(p, smallArr,callback);
        }else{
          callback(that.baiduArr);
        }
       },
       fail: function(){
         that.isTapSelected = false;
       }

     })
   
 },
  //搜索点击
  tapSearch: function (e) {
    console.log(">>>>>>");
  },
  //mark点击事件   点击marker时isClickMarker为true  其他为false
  markertap: function (e) {
    console.log(e.markerId + "点击marker");
    var that = this;
    that.setData({
      isClickMarker: true
    });
    var id = e.markerId;
    that.comeBigMark(id);
  },
  //放大Mark
  comeBigMark: function (id) {
    var that = this;
    var markersArr = that.data.markers;
    for (var i = 0; i < markersArr.length; i++) {
      console.log(id + "----编号" + markersArr[i].id);
      
      if (id == markersArr[i].id) {
        that.setMarkerInfo(that.data.allmarkerDatainfo[i]);
        markersArr[i].width = 61.2,
          markersArr[i].height = 68.2
      } else {
        if (that.data.isHaveStake == 1){
          markersArr[i].width = 45.9;
          markersArr[i].height = 51.3
        }else{
          markersArr[i].width = 36;
          markersArr[i].height = 37
        }
      }
    }
    this.setData({
      markers: markersArr,
    });

  },
  //点击按钮跳转页面
  controltap: function (e) {
    this.setUnTouch();
    var that = this;
    console.log(e.controlId);
    var _id = e.controlId;
    switch (_id) {
      case 1:
        that.getUserLocation();
        break;
      case 2: {
        scan_code.scan_code(function (res) {
          wx.removeStorageSync("isTemporaryBike");
          if (res.rentType == "1") {
            that.setData({
              isHireStake: 1,
              isRentbike: true
            });
          } else {
            that.setData({
              isHireStake: 3,
              isRentbike: true
            });
          }
          wx.reportAnalytics('rentbike', {
           
          })
        });
      }
        break;
      case 3:
        this.tapStake();
        break;
      case 4:
        this.getcenter();
        break;
      case 6:
        this.flashFindBike();
        break;
      case 7:
        break;
    }
  },
  //跳转搜索页面
  searchAction: function () {
    var that = this;
    wx.navigateTo({
      url: '../search/search?stake=' + that.data.isHaveStake,
    });
  },
  //点击用户按钮
  getcenter: function () {
    var that = this;

    var bizinfo = wx.getStorageSync("bizinfo");
    if (bizinfo.retcode == "0") {
      wx.navigateTo({
        url: `../myCenter/myCenter`
      })
    } else {
      var _title = '您尚未登录';
      var backurl = '../login/login?backPage=../myCenter/myCenter';
      that.toLoginMsg(_title, backurl);
    }
  },
  //消息提示框显示隐藏
  showMessage: function (msg) {
    wx.showLoading({
      title: msg,
    });
    setTimeout(function () {
      wx.hideLoading()
    }, 1000)
  },
  //分享
  onShareAppMessage: function () {
    return {
      title: '智骑江北，绿色出行',
      path: '/pages/index/index'
    }
  },
  //未登录提示框跳转
  toLoginMsg: function (_title, backurl) {
    wx.navigateTo({
      url: backurl
    });
  },
  //给markerdata（markers所有的数据）属性赋值  距离、可借、可还
  setMarkerInfo: function (currentMarker) {
    var that = this;
    var startLon = that.data.centerLon;
    var startLat = that.data.centerLat;
    var tmpdistance = 0;
    if (that.data.isHaveStake == 1) {
      if (currentMarker.rentcount) {
        that.getPointDistance(startLon, startLat, currentMarker.lon, currentMarker.lat, function (distance) {
          that.setTopMarkView(distance, currentMarker);
        });
      } else {
        apiM.getStation(currentMarker.id, "1", function (stationinf) {
          that.getPointDistance(startLon, startLat, stationinf.lon, stationinf.lat, function (distance) {
            that.setTopMarkView(distance, stationinf);
          });
        });
      }
    } else {
      var lonlatstring = currentMarker.coordinate.split(",");
      that.getPointDistance(startLon, startLat, Number(lonlatstring[0]), Number(lonlatstring[1]), function (distance) {
        that.setTopMarkView(distance, currentMarker);
      });
    }
  },
  //设置顶部站点信息显示 
  setTopMarkView: function (distance, currentmarkerInf) {
    var that = this;
    if (distance > 1000) {
      unit = "千米";
      distance = (distance / 1000).toFixed(2);
    } else {
      var unit = "米";
      distance = parseInt(distance);
    }
    if (that.data.isHaveStake == 1) {
      that.setData({
        uNit: unit,
        markerdata: {
          distance: distance,
          rentcount: currentmarkerInf.rentcount,
          restorecount: currentmarkerInf.restorecount,
          lon: currentmarkerInf.lon,
          lat: currentmarkerInf.lat,
          bikebnumber: currentmarkerInf.number,
          name: currentmarkerInf.name + '('+currentmarkerInf.number+')'
        }
      });
    } else if (that.data.isHaveStake == 3) {
      var longstring = currentmarkerInf.coordinate.split(",");
      that.setData({
        uNit: unit,
        markerdata: {
          distance: distance,
          lon: Number(longstring[0]),
          lat: Number(longstring[1]),
          name: currentmarkerInf.deviceName + '(' + currentmarkerInf.deviceId + ')'
        }
      });
      console.log(longstring);
    }
  },
  //视野发生变化，设置黄色地图中心点
  regionchange: function (e) {
    console.log("视野发生变化，设置黄色地图中心点");
    var that = this;
    console.log(e.type);
    if (e.type == 'end') {
      that.mapCtx.getCenterLocation({
        success: function (res) {
          that.baiduToTxPoint([res.latitude+',' + res.longitude], function (location) {
            that.setData({
              centerLat: location[0].lat,
              centerLon: location[0].lng,
            });
            that.getPointDistance(location[0].lat, location[0].lng, that.data.lastpointLat, that.data.lastpointLon, function (distance) {
              if (distance >= 500 && that.data.isHaveStake == 1) {
                that.setData({
                  lastpointLon: location[0].lng,
                  lastpointLat: location[0].lat
                });
                that.getNearStgetneaation();
                console.log("移动大于500 加载附近站点" + distance);
              } else {
                console.log("移动小于500 不加载附近站点" + distance);
              }
            });

          });
        
        },
        fail: function (res) {
          console.log(res);
        }
      });
    }
  },
  //点击地图  清除marker悬浮框 显示搜索框
  clickmapbtn: function () {
    var that = this;
    if (that.data.isClickMarker == true) {
      that.comeBigMark(999999999);
    }
    that.setData({
      isClickMarker: false
    })
  },
  //不可再点击
  setUnTouch: function () {
    this.setData({
      userCanTouch: true
    })
  },
  //导航
  hasStakeGpsBtn: function () {
    this.setUnTouch();
    var that = this;

    wx.navigateTo({
      url: '../guideRoad/guideRoad?startlon=' + that.data.centerLon + '&startlat=' + that.data.centerLat + '&endlat=' + that.data.markerdata.lat + '&endlon=' + that.data.markerdata.lon + '&rentcount=' + that.data.markerdata.rentcount + '&restorecount=' + that.data.markerdata.restorecount + '&name=' + that.data.markerdata.name + '&isHaveStake=' + that.data.isHaveStake
    })

  },
  //计算距离
  calculateDistance: function (startLon, startLat, endLon, endLat, callback) {
    var that = this;
    console.log(startLon + "---" + startLat + "---" + endLon + "+++" + endLat)
    demo.calculateDistance({
      from: {
        latitude: startLat,
        longitude: startLon
      },
      to: [{
        latitude: endLat,
        longitude: endLon
      }],
      success: function (res) {
        var distance = res.result.elements[0].distance;
        callback(parseInt(distance));

      },
      fail: function (res) {

        console.log(res);
      },
      complete: function (res) {
        console.log(res);
      }
    });
  },

  ///计算两点间的直线距离
  getPointDistance: function (lat1, lng1, lat2, lng2, callback) {
    lat1 = lat1 || 0;
    lng1 = lng1 || 0;
    lat2 = lat2 || 0;
    lng2 = lng2 || 0;

    var rad1 = lat1 * Math.PI / 180.0;
    var rad2 = lat2 * Math.PI / 180.0;
    var a = rad1 - rad2;
    var b = lng1 * Math.PI / 180.0 - lng2 * Math.PI / 180.0;

    var r = 6378137;
    callback(r * 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(rad1) * Math.cos(rad2) * Math.pow(Math.sin(b / 2), 2))));
  },
  /// 车辆故障
  rentBikeError: function () {
    var that = this;
    apiM.getBizInfo(function (bizinfo) {
      wx.setStorageSync("bizinfo", bizinfo);
      that.initUserStatus();
    })
  },
  //临时停车
  temporaryBike: function () {
    var that = this;
    scan_code.temporaryParking(function (res) {
      wx.setStorageSync("isTemporaryBike", "true")
      that.setData({
        isTmpStop: true,
      })
    })

  },
  //继续骑行
  continueBike: function () {
    var that = this;
    scan_code.continueBike(function (res) {
      wx.removeStorageSync("isTemporaryBike");
      that.setData({
        isTmpStop: false,
      })
    })
  },
  //还车
  restoreBike: function () {
    var that = this;
    scan_code.lockBike(function (res) {
      wx.removeStorageSync("isTemporaryBike");
      that.initUserStatus();
    });
  },
  //前往支付
  goTOpay: function () {
    this.setUnTouch();
    wx.navigateTo({
      url: '../order/order',
    })
  },
  //版本判断
  updatesysteminfo: function () {
    wx.getSystemInfo({
      success: function (res) {
        var verArr = res.version.split('.');
        var systermOr = res.system;
        var tmpversion = verArr[0];
        for (var i = 1; i < verArr.length; i++) {
          tmpversion = tmpversion + verArr[i];
        }
        console.log(tmpversion);
        if ((tmpversion < 656 && systermOr == 'ios') || (tmpversion <= 659 && systermOr == 'android')) {
          wx.showModal({
            title: '提示',
            content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
          });
        }
      },
    })
  }

})
module.exports = {
  currentmarkerInfo: currentmarkerInfo
}


