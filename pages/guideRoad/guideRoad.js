 代理地址https://wxapi.hotapp.cn/proxy/?appkey=hotapp123073051&url=https
//获取应用实例
// 引用百度地图微信小程序JSAPI模块 
var bmap = require('../../utils/bmap-wx.min.js');
var wxMarkerData = [];
var app = getApp();
var QQMapWX = require('../../utils/qqmap-wx-jssdk.js');
var uTil = require('../../utils/util.js');
var wxMarkerData = [];
var demo = new QQMapWX({
  key: 'T6DBZ-GJBWP-OECDP-VQV46-CXBFO-HZFLS' // 必填
});
var indexObject=require('../index/index.js');
Page({
  data: {
    markers: [],
    polyline: [],
    longitude:null,
    latitude:null,
    cliheight:null,
    sitename:"",
    kilometer:"",
    rent:"",
    restore:"",
    endlon: "",
    endlat: "" ,
    distance:null,
    isHasStakeShow:1,
    address: ""
  },
  onLoad: function (options) {
    var that=this;
    console.log(options.startlon + "," + options.startlat + "," + options.endlat + "," + options.endlon + "," + options.rentcount+","+
      options.name + "," + options.restorecount + options.address + options.isHaveStake);
    
    var that =this;
    that.init_map(options);
    that.calDistance();
    
  },
  init_map: function (options){
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          cliheight: res.windowHeight * 0.8
        });
      }
    })
    that.setData({
      polyline: [{
        points: [{
          longitude: options.startlon,
          latitude: options.startlat
        }, {
          longitude: options.endlon,
          latitude: options.endlat
        }],
        color: "#FF0000DD",
        width: 2,
        dottedLine: true
      }],
      latitude: options.startlat,
      longitude: options.startlon,
      markers: [
        {
          height: 32,
          width: 23,
          iconPath: '../../img/icon_start0@2x.png',
          latitude: options.startlat,
          longitude: options.startlon
        },
        {
          height: 32,
          width: 23,
          iconPath: '../../img/icon_end0@2x.png',
          latitude: options.endlat,
          longitude: options.endlon
        }
      ],
      
      endlon: options.endlon,
      endlat: options.endlat
    });
    that.stakeJudge(options);
    console.log(that.data.endlon + "," + that.data.endlat + "终点经纬度");
  },
  stakeJudge: function (options){
   var that=this;
   if (options.isHaveStake==1){
     that.setData({
       isHasStakeShow:1,
       sitename: options.name,
       rent: options.rentcount,
       restore: options.restorecount
     })
   } else if (options.isHaveStake == 3){
     that.setData({
       isHasStakeShow:3,
       sitename: options.name
     });
   }
  },
  begin:function(e){
      var that = this;
      console.log(that.data.markers[1].latitude + "," + that.data.markers[1].longitude);
        wx.openLocation({
          latitude: parseFloat(that.data.markers[1].latitude), 
          longitude: parseFloat(that.data.markers[1].longitude),
          scale: 14,
          name: that.data.sitename, 
          address:'点击右边按钮即可导航哦',
          success:function(res){
            console.log("success" + res+"开始导航");
          },
          fail:function(){
            console.log("fail"+"开始导航");
          }
        });     
  },
  onReady: function (e) {
    // 页面渲染完成  
    this.mapCtx = wx.createMapContext('myMap');
  },
  calDistance:function(){
    var that = this; 
    console.log(that.data.endlon + "," + that.data.endlat);
    that.calculateDistance(that.data.longitude, that.data.latitude, that.data.endlon, that.data.endlat,function(distance){
      
      if (distance<1000){
        that.setData({
          kilometer:"m",
          distance: distance
        })
      }else{
        that.setData({
          kilometer: "千米",
          distance:  (distance / 1000).toFixed(2)
        });
      }   
    });
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
 
  ///计算距离
  calculateDistance: function (startLon, startLat, endLon, endLat, callback) {
    console.log(endLon + "," + endLon + "," + String(endLon));
    var that = this;
    demo.calculateDistance({
      from: {
        latitude: String(startLat),
        longitude: String(startLon)
      },
      to: [{
        latitude: String(endLat),
        longitude: String(endLon)
      }],
      success: function (res) {
        var distance = res.result.elements[0].distance;
        callback(parseInt(distance));
        console.log(parseInt(distance) + "success距离");
      },
      fail: function (res) {
        if (res.status == 373){
          callback(10000);
        }
        console.log(res);
      },
      complete: function (res) {
       
      }
    });
  }
})
  