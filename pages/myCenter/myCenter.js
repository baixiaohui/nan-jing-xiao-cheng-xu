//myCenter.js
var app = getApp();
var uTil = require('../../utils/util.js');
var apiM = require('../../utils/APIManager.js');
var scan_code = require('../../utils/scan_qrcode.js');

Page({
  data: {
    userInfo: {},
    curname:'********',
    curphone: null,
   array:[
     {
        "id":1,
        "name":"保证金"
     },
     
     {
       "id": 2,
       "name": "用户指南"
     },
    //  {
    //    "id": 3,
    //    "name": "使用说明"
    //  },
    //  {
    //    "id": 4,
    //    "name": "关于我们"
    //  },
     {
       "id": 5,
       "name": "行程记录"
// 'name':'租车'
     },
     {
       "id": 6,
       "name":"订单详情"
      //  'name': '还车'
     }
      ],
       src:""
  },
  
  onLoad: function () {
   
    wx.stopPullDownRefresh();
       //调用应用实例的方法获取全局数据
       var that=this;
       
       if (wx.getStorageSync('bizinfo').retcode == "0"){
            var wxuserinfo = wx.getStorageSync("wxuserinfo");
            
              var bizinf = wx.getStorageSync('bizinfo');
              if (bizinf){
                console.log(bizinf);
                var tel = bizinf.phonenum.substring(0, 3) + "****" + bizinf.phonenum.substring(7, 11);
                var nickName = bizinf.userid;
                var avatarUrl = wxuserinfo.avatarUrl;
                that.setData({
                  curname: nickName,
                  src: avatarUrl,
                  curphone: tel
                });
              }else {
                apiM.getBizInfo(function (bizinf){
                  var tel = bizinf.phonenum.substring(0, 3) + "****" + bizinf.phonenum.substring(7, 11);
                  var nickName = bizinf.userid;
                  var avatarUrl = wxuserinfo.avatarUrl;
                  that.setData({
                    curname: nickName,
                    src: avatarUrl,
                    curphone: tel
                  });
                });
              }
              
          
        }
    },
    chosen:function(e){
       console.log(e.currentTarget.dataset.id);
       var that = this;
       var _id=e.currentTarget.dataset.id;
       console.log(_id+"选择结果")
       switch (_id) {
         case 1:
           wx.navigateTo({
             url: '../deposit/deposit',
           });
           break;
         case 2:
           console.log( "选择结果2 跳转")
           wx.navigateTo({
             url: '../userguide/userguide',
           });
           break;
        
         case 3:
           wx.navigateTo({
             url: '../useInstruciton/useInstruciton',
           });
           break;
         case 4:
           wx.navigateTo({
             url: '../aboutus/aboutus',
           });
           break;
         case 5:
           wx.navigateTo({
             url: '../tripRecord/tripRecord',
           });
          //  scan_code.scan_code(function (res) {
           
          //  });
           break;
          case 6:
           wx.navigateTo({
             url: '../order/order',
           });
          //  scan_code.lockBike(function (res) {
          //  });
           break;
       }    
    },
    //退出登录
    quitLogin: function () {
      var that = this;
      wx.showModal({
        title: '提示',
        content: '亲，您确定要退出登录吗?',
        success: function (res) {
          if (res.confirm) {
            that.loginOutAction();
          }
        }
      })
    },
    loginOutAction: function () {
      var that = this;
      apiM.loginOut(function (res) {
        console.log(res.retcode + "getOrdersAct");
        console.log(res);
        if (res.retcode == "0") {
          apiM.getBizInfo(function (bizinfo) {
              wx.setStorageSync("bizinfo", bizinfo);
            });
          uTil.showMsg('退出中...', '../index/index');
          wx.removeStorageSync("isTemporaryBike");
        }
      });
    },
    onReady:function(){
        // 页面渲染完成 
    },
    onShow:function(){

        // 页面显示
    },
    onHide:function(){
        // 页面隐藏
    },
    onUnload:function(){
        // 页面关闭
    },
})
